# Protocols and Analysis tools for PEG Solubility Assay

This repository accompanies the paper 'An open-source automated PEG-precipitation assay to measure protein relative 
solubility with low material requirement'. It contains protocols to be used with the Opentrons OT-2 pipetting robot and
all scripts and a jupyter notebook to analyse the data. Moreover, example files are given.

### PEG_Assay_Step1.py:
This protocol transfers buffer, PEG and protein into the well plate. The user can specify, the target peg concentrations 
from 0 – 33% (around 12 concentrations are recommended to cover a broad range of concentrations), number of proteins, 
number of controls, number of replicates, number of conditions (the standard protocol runs with one protein at one 
condition, one control and two replicates). Although it is possible to use this protocol with more than one protein at 
once, it is not recommended to use it with more than two since the transfer of the supernatant using the 
PEG_Assay_Step2.py protocol takes around 40 min per protein risking resolubilisation of the precipitate if too many 
proteins are used at once. The buffer is stored in a 15 mL Falcon tube, all three PEG stocks are stored in 1.5 mL 
Eppendorf tubes and the protein is stored in a 1.5 mL Eppendorf tube. To reduce the amount of material used as much as 
possible it is recommended to use 384 low volume plates with a working volume of 4 – 25 µL (Greiner, UV star microplate 
384 r3f 788876). With these plates, final volumes of 10 µL per well are feasible. The protocol adds the buffer first, 
followed by PEG and finally the protein. To ensure that the correct amount of PEG is transferred, several modifications
to the standard transfer procedure are applied: reduction of the vertical speed of the pipette; reduction of the 
aspiration and dispensing speed; pausing after aspirating/dispensing to ensure all of the PEG is aspirated/dispensed; 
touching the tip at the top of the Eppendorf tube to get rid of all drops that are on the outside of the tip; 
aspirating air after aspirating PEG to ensure no PEG is lost due to gravitational pull; blow out after dispensing to
ensure no PEG is left in the tip. 

### PEG_Assay_Step2.py:
This protocol is used to transfer the supernatant from the plate after centrifugation into a fresh plate. Before the 
supernatant is added, buffer is added to the target wells to ensure that the bottom of each well is covered for error-
free analysis using the plate reader (9 µL in the standard protocol, yielding a final volume of 12 µL after addition of 
3 µL of supernatant). The protocol can be started before the centrifugation is finished. The robot pauses automatically 
after transferring the buffer to give the user the opportunity to put the source plate into the robot. For each well 
from the original plate, it transfers a specified amount into two different wells of the target plate. Due to 
evaporation during the incubation only around 8 µL are left in each well. It is recommended to transfer 2x 3 µL. 
This ensures that no precipitant is aspirated. The user can specify which wells to transfer from and to.  The accuracy 
of this step is again greatly enhanced by using an automated process. By accurately calibrating the pipettes of the 
robot it is possible to avoid any aspiration of precipitant. If the pipette of the robot moves too deep into the well, 
it is possible to pick up precipitate whereas if it doesn’t move deep enough, it won’t be able to aspirate the correct 
amount of supernatant. 

### Make_Plate_Layout.py
PEG_Assay_Step1.py and PEG_Assay_Step2.py output a dictionary which contains information on the plate layout. This 
script transforms this dictionary into a csv file visualising the plate layout.

### Adjust_Plate_Reader_Data.py
This script adds the information of the plate layout to the output file from a plate reader. It returns a sorted csv 
file that contains information about the content of each well (i.e. control or protein, peg concentration). It also 
applies Savgol filter to smooth the data and reduce the noise.


### Jupyter notebook – Plate Reader Analysis
This notebook guides the user through the analysis of the experimental data. Details about each function are given in 
the notebook. Here, only a brief overview over the different steps is given. 
At first, the pre-processed raw data is loaded in. The input should be a csv file containing the following information 
for each well: PEG concentration, whether it contains protein or a control and absorbance values for all wavelengths 
measured. Then the user is prompted to run a script that identifies false readings that should be excluded from further 
analysis. Subsequently, the soluble protein concentration against PEG concentration is plotted. This is done by 
calculating the concentration at 280 nm while controlling for the absorbance at 340 nm. Finally, the sigmoidal fit is 
performed. The function being fitted is the following:

![\Large \bigmy=\frac{a-b}{1+e^{s\left(x-PEG_{1/2}\right)}}+b](https://latex.codecogs.com/svg.latex?y=\frac{a-b}{1+e^{s\left(x-PEG_{1/2}\right)}}+b) 

where a and b constitute the upper and lower plateaus, s is the slope of the curve and PEG1/2\ is the inflection point.

The fit contains several steps. After fitting an initial sigmoidal function, the data is normalised by using the upper 
and lower plateaus. With the normalised data 95% confidence intervals are calculated with 500 bootstrap cycles. The 
notebook also contains functions to analyse turbidity, intrinsic fluorescence and the experiment with free fluorophore
AlexaFluor488.  
