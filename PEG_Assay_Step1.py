"""
Protocol for the first step of the PEG solubility assay. This protocol is used to titrate PEG into a low-volume 384
well plate followed by the addition of protein.
"""

from opentrons import protocol_api
from opentrons.types import Location
from opentrons.protocol_api.labware import Well  # API2 WellSeries is not present in API2 it is just a list
import types, sys
import numpy

#######################################################################################
#
#   FUNCTION LIBRARY - scroll down to next break to edit samples and protocols
#
#######################################################################################

collated_warnings = []

PI = 3.141593


class tmp_writer:
    def comment(msg):
        sys.stdout.write(msg)
        sys.stdout.flush()

    def pause(msg):
        raise Exception("**ERROR** pause not working in tmp_writer mode\n")
        sys.stderr.flush()


aux = {'protocol_context': tmp_writer}

def print_warning(message, list_of_warnings=collated_warnings):
    '''
    prints a warning to the log and also adds it to the list so
     that all can be printed at the end as well
    '''
    print(message)
    aux['protocol_context'].comment(message)
    list_of_warnings += [message]
    return


def volume_to_liquid_level_15Falcon(liquidVolumeInside_uL, cone_h_mm=22.6, diam_mm=14.3, cone_correction=2.55):
    '''
    calculate liquid level (in mm from bottom of tube) by knowing the liquid volume in the tube
    # 15 mL; inner diameter 15.62 mm; total height 118.61-10.16(cap); cone height at bottom is 23.36
    (assuming 22.6 mm inner) https://www.viaglobalhealth.com/wp-content/uploads/corning-docs/t_centrifuge_tube_insert_DL.pdf
    # area=(PI*((diam_mm/2)**2)); totV=area*cone_h_mm / cone_correction + ( 117.98-cone_h_mm)*area
    '''
    if liquidVolumeInside_uL > 16000:
        raise ValueError("volume_to_liquid_level_15Falcon input volume %lf >16000uL\n" % (liquidVolumeInside_uL))
    area = (PI * ((diam_mm / 2) ** 2))
    coneV = area * cone_h_mm / cone_correction
    if liquidVolumeInside_uL <= coneV:  # volume of cone with 2.9 instead of 3
        h = cone_correction * liquidVolumeInside_uL / area  # cone height - 2.9 should be 3 but this correct for the fact it likely not really a cone and holds more volume
    else:
        h = cone_h_mm + (liquidVolumeInside_uL - coneV) / area  # cylinder height
    return h - 1 # -1 as a buffer to ensure that the tip doesn't crash into the bottom of the tube


def volume_to_liquid_level_50Falcon(liquidVolumeInside_uL, cone_h_mm=15.3, diam_mm=26.2, cone_correction=2.2):
    '''
    calculate liquid level (in mm from bottom of tube) by knowing the liquid volume in the tube
    # 50 mL; inner diameter is 27.94; height total is 114.65; cone height 15.88 and cap 12.31
    (assuming innner cone height 15.3) https://www.viaglobalhealth.com/wp-content/uploads/corning-docs/t_centrifuge_tube_insert_DL.pdf
    '''
    if liquidVolumeInside_uL > 50000:
        raise ValueError("volume_to_liquid_level_50Falcon input volume %lf >50000\n" % (liquidVolumeInside_uL))
    area = (PI * ((diam_mm / 2) ** 2))
    coneV = area * cone_h_mm / cone_correction
    if liquidVolumeInside_uL <= coneV:  # volume of cone with 2.9 instead of 3
        h = cone_correction * liquidVolumeInside_uL / area  # cone height - 2.9 should be 3 but this correct for the fact it likely not really a cone and holds more volume
    else:
        h = cone_h_mm + (liquidVolumeInside_uL - coneV) / area  # cylinder height
    return h - 1 # -1 as a buffer to ensure that the tip doesn't crash into the bottom of the tube


def volume_to_liquid_level_PCR(liquidVolumeInside_uL, cone_h_mm=11, diam_mm=5.4, cone_correction=2.7):
    '''
    calculate liquid level (in mm from bottom of tube) by knowing the liquid volume in the tube
    opentrons-aluminum-block-PCR-strips-200ul for normal pcr tubes 20.3mm tall (300uL), UNTESTED
    opentrons-aluminum-block-96-PCR-plate for short pcr 14.81 mm tall (200 uL), use  volume_to_liquid_level_PCRplateLowProfile in this case!
    '''
    if liquidVolumeInside_uL > 300:
        raise ValueError("volume_to_liquid_level_PCR input volume %lf >300uL\n" % (liquidVolumeInside_uL))
    area = (PI * ((diam_mm / 2) ** 2))
    coneV = area * cone_h_mm / cone_correction
    if liquidVolumeInside_uL <= coneV:  # volume of cone with 2.9 instead of 3
        h = cone_correction * liquidVolumeInside_uL / area  # cone height - 2.9 should be 3 but this correct for the fact it likely not really a cone and holds more volume
    else:
        h = cone_h_mm + (liquidVolumeInside_uL - coneV) / area  # cilinder height
    return h

def volume_to_liquid_level_PCRplateLowProfile(liquidVolumeInside_uL, cone_h_mm=14.71, diam_mm=6.4, cone_correction=2.5):
    '''
    calculate liquid level (in mm from bottom of tube) by knowing the liquid volume in the tube
    # opentrons-aluminum-block-96-PCR-plate for biorad unskirted low profile plates (e.g. http://www.bio-rad.com/es-es/sku/mll9651-multiplate-96-well-pcr-plates-low-profile-unskirted-white?ID=MLL9651) well height 'depth': 14.81 (on biorad it says overall height (15.50 mm) low-volume reactions (5–125 µl; 200 µl max per well)
    '''
    if liquidVolumeInside_uL > 300:
        raise ValueError("volume_to_liquid_level_PCR input volume %lf >300uL\n" % (liquidVolumeInside_uL))
    area = (PI * ((diam_mm / 2) ** 2))
    coneV = area * cone_h_mm / cone_correction
    if liquidVolumeInside_uL <= coneV:  # volume of cone with 2.9 instead of 3
        h = cone_correction * liquidVolumeInside_uL / area  # cone height - 2.9 should be 3 but this correct for the fact it likely not really a cone and holds more volume
    else:
        h = cone_h_mm + (liquidVolumeInside_uL - coneV) / area  # cilinder height
    return h


def volume_to_liquid_level_1p5Eppendorf(liquidVolumeInside_uL, area=PI * ((8.7 / 2) ** 2)):
    '''
    calculate liquid level (in mm from bottom of tube) by knowing the liquid volume in the tube
    Distance between 500 uL marking (end of cone) and 1 mL tick (mid of cylinder) is 8.2 mm suggesting a diameter of 8.8
    compatible with 8.7 in new labware defintion
    #Dimensions: Volume 1.5 mL; Inner Diameter 8.7 mm; Outer Diameter 10 mm; Height 37.8 mm.
    As marking at 500 uL is roughly at the end of the cone; suggests cone height is 28.4 mm which is clearly wrong
    (18mm is about right), probably because not really a cone..
    I was expecting something around 16.8 (2/5 of total height) if we use this we can estimate the cone volume of 500 uL
    by using a factor 2. instead of 3 also for safety
    Note that with diameter of 8.7 in the range 0 - 500 uL, h of a cone with factor 2 instead of 3 goes like that of a
    truncated cone with smaller diameter 3.8 mm !!
    Cone volume is PI*r**2 *h/3  ==> h= 3*V/(PI*(d/2)**2)
    Truncated cone  volume V = PI * h/3 * (r**2 + r * R + R**2) ==> h= 3*V/(PI* (r**2 + r * R + R**2))
    '''
    if liquidVolumeInside_uL > 1700:
        raise ValueError("Volume_to_liquid_level_1p5Eppendorf input volume %lf >1700uL\n" % (liquidVolumeInside_uL))
    if liquidVolumeInside_uL < 500:  # volume of cone - measured cone height in 1.8 cm measured diameter is 8.7 mm
        h = 2. * liquidVolumeInside_uL / (area)  # cone height - factor 2 instead of 3 approximates truncated cone in this range.
    else:
        h = 16.8 + (liquidVolumeInside_uL - 500) / (area)  # cylinder height
    return h


def positive(x):
    if x < 0:
        return 0
    return x


def auto_liquid_level(well, assume_cylinder=True, print_warns=True):
    '''
    Automatically choose the appropriate function to convert volume to liquid level from the input tube type
    well can be a tube or any other container
    for tubes that don't have a default function (those written above)
    assume_cylinder will assume the well is perfectly cylindrical, and will calculate the level
     using the dimensions in well.properties. This is typically a safe approach since most wells don't have a flat
     bottom and the liquid level is therefore higher than the calculated level ensuring that the tip will always be
     submerged
    '''

    volume = props[well].properties['filled_volume']  # assume its existence has already been checked
    if volume <= 0:
        return 1  # 1 mm above bottom as by default
    if '1.5ml-eppendorf' in repr(well) or 'eppendorf_1.5ml' in repr(well):
        return volume_to_liquid_level_1p5Eppendorf(volume)
    elif well.max_volume == 15000 and ('tuberack-15' in repr(well) or 'Falcon 15' in repr(well)):  # assume 15 mL Falcon,Api1 and Api2
        return volume_to_liquid_level_15Falcon(volume)
    elif well.max_volume == 50000 and ('50ml' in repr(well) or 'Falcon 50' in repr(well)):  # assume 50 mL Falcon, Api1 and Api2
        return volume_to_liquid_level_50Falcon(volume)
    elif '-PCR-STRIPS-200UL' in repr(well).upper() or ('PCR' in repr(well).upper() and '200' in repr(well)):  # assume PCR tubes
        return volume_to_liquid_level_PCR(volume)
    elif '-PCR-PLATE' in repr(well).upper() or ('PCR' in repr(well).upper() and '100' in repr(well)):  # assume biorad plate low profile
        return volume_to_liquid_level_PCRplateLowProfile(volume)
    elif props[well].properties['shape']=='square' and 'width' in props[well].properties and 'length' in props[well].properties and 'depth' in props[well].properties:  # assume squared well
        if print_warns and 'warned' not in props[well].properties:
            aux['protocol_context'].comment(
                "Note container %s assuming squared/rectangular shape to calculate liquid height from volume, using sides of %g and %g and depth %g\n" % (
                repr(well), props[well].properties['width'], props[well].properties['length'], props[well].properties['depth']))
            props[well].properties['warned'] = True  # this well should not give further warnings
        h = volume / (props[well].properties['length'] * props[well].properties['width'])
        if props[well].properties['depth'] > h:
            return h
        else:
            return props[well].properties['depth'] / 2.  # a bit arbitrary but should never happen
    elif assume_cylinder and 'diameter' in props[well].properties and 'depth' in props[well].properties:
        if print_warns and 'warned' not in props[well].properties:
            aux['protocol_context'].comment(
                "Note container %s assuming cylindrical shape to calculate liquid height from volume, using diameter %g and depth %g\n" % (
                repr(well), props[well].properties['diameter'], props[well].properties['depth']))
            props[well].properties['warned'] = True  # this well should not give further warnings
        h = volume / (PI * ((props[well].properties['diameter']) / 2.) ** 2)
        if props[well].properties['depth'] > h:
            return h
        else:
            return props[well].properties['depth'] / 2.  # a bit arbitrary but should never happen

    else:
        if print_warns and 'warned' not in props[well].properties:
            aux['protocol_context'].comment(
                "*Warn* container %s cannot calculate liquid height from volume returning default 1mm\n" % (repr(well)))
            props[well].properties['warned'] = True  # this well should not give further warnings
    return 0


def optimize_source_height(volume, source, offset=0, debug=False):
    '''
    return volume,source_optimisedZlevel
    so it can be used like e.g.  :
    pipette.aspirate( *optimize_source_height(30, plate.wells('A1')) )
      to make the pipette aspirate volume (30 uL in the example) from source well ('A1' in the example)
      by setting the pipette at an height that is sufficient to get the volume but not too deep to
      risk leaving too much liquid on the outside of the pipette
    '''
    Z = None
    if not isinstance(source, Well):
        if isinstance(source, Location):  # API2
            Z = source
            source = source.labware  # well object hopefully
        elif type(source) is tuple and len(
                source) == 2:  # Probably already coordinates like .top() or .bottom() e.g. plate.well(0).top() = (<Deck><Slot 7><Container corning_96_wellplate_360ul_flat><Well A1>, (x=3.20, y=3.20, z=10.50))
            Z = source
            source = source[0]
        else:
            print_warning(
                "**ERROR** in optimize_source_height, input source %s not of type placeable.Well\n" % (repr(source)))
    if 'filled_volume' not in props[source].properties:  # returnd default - will likely set tip to 1mm above bottom of well/tube
        if 'warned' not in props[source].properties:
            print_warning(
                "**WARNING** Source container %s has no pre-defined 'filled_volume' - using default z=1mm tip position\n" % (
                    repr(source)))
            props[source].properties['warned'] = True  # this well should not give further warnings
        if debug: aux['protocol_context'].comment(
            ' optimize_source_height: %s new volume is %g uL' % (repr(source), source.properties['filled_volume']))
        if Z is None:
            Z = source
        return volume, Z
    else:  # reduce volume and calculate liquid height so that tip will have enough volume to aspirate above it
        if props[source].properties['filled_volume'] - volume < 0:
            print_warning(
                "**ERROR** Source container %s is NOW EMPTY accoding to liquid tracking - trying to transfer %g but likely transferring only %g volume\n" % (
                repr(source), volume, positive(props[source].properties['filled_volume'])))
        props[source].properties['filled_volume'] -= volume
        if Z is None:
            Z = source.bottom(max([0.5, auto_liquid_level(source) + offset]))
        if debug: aux['protocol_context'].comment(
            ' optimize_source_height: %s new volume is %g uL  liquid level=%g mm offset=%g mm Z=%s' % (
            repr(source), props[source].properties['filled_volume'], auto_liquid_level(source), offset, repr(Z)))
        return volume, Z


def optimize_destination_height(volume, destination, offset=0, debug=False):
    '''
    return volume,destination_optimisedZlevel
    so it can be used like e.g. :
    pipette.dispense( *optimize_destination_height(30, plate.wells('A1')) )
      to make the pipette dispense volume (30 uL in the example) into destination well ('A1' in the example)
      by setting the pipette at an height that is sufficient to touch the destination liquid
      but not too deep to risk leaving too much liquid on the outside of the pipette
    '''
    Z = None
    if not isinstance(destination, Well):
        if isinstance(destination, Location):  # API2
            Z = destination
            destination = destination.labware  # well object hopefully
        elif type(destination) is tuple and len(
                destination) == 2:  # API1 already coordinates like .top() or .bottom() e.g. plate.well(0).top() = (<Deck><Slot 7><Container corning_96_wellplate_360ul_flat><Well A1>, (x=3.20, y=3.20, z=10.50))
            Z = destination
            destination = destination[0]
        else:
            print_warning(
                "**ERROR** in optimize_destination_height, input destination %s not of type placeable.Well\n" % (
                    repr(destination)))
    if 'filled_volume' not in props[destination].properties:  # Set to Zero+volume and returnd default - will likely set tip to 1mm above bottom of well/tube
        props[destination].properties[
            'filled_volume'] = volume  # we assume this was empty, afer transfer it will contain volume
        if debug:
            aux['protocol_context'].comment(' optimize_destination_height: %s new volume is %g uL' % (repr(destination), destination.properties['filled_volume']))
        if Z is None:
            Z = destination
        return volume, Z  # this will be default so should stop 1mm above bottom of well.
    else:  # increase volume and calculate liquid height so that tip will have enough volume to aspirate above it
        if props[destination].properties['filled_volume'] + volume > destination.max_volume:
            print_warning(
                "**WARNING** destination container %s is OVERFLOWING accoding to liquid tracking - current volume is %g uL and adding %g uL and max volume %g uL\n" % (
                repr(destination), props[destination].properties['filled_volume'], volume, destination.max_volume))
        if Z is None:
            Z = destination.bottom(max([1, auto_liquid_level(destination) + offset]))
        props[destination].properties['filled_volume'] += volume  # change after getting the height as we want to deposit with the tip inside the well
        if debug:
            aux['protocol_context'].comment(' optimize_destination_height: %s new volume is %g uL liquid level= %g mm offset= %g mm' % (
            repr(destination), props[destination].properties['filled_volume'], auto_liquid_level(destination), offset))
        return volume, Z


def transfer_liquid_height(volume, source, destination, source_level_safety=0):
    '''
    return volume,source_optimisedZlevel,destination_optimisedZlevel
    so it can be used like e.g. :
    pipette.transfer( *transfer_liquid_height(30, plate.api1wells('A1'), plate.api1wells('A2')) )
    source_level_safety should be a positive number that will go source_level_safety below the liquid level in the source to make sure no air is aspired
    '''
    if type(source) is tuple or type(source) is list:
        Osource = []
        for j, well in enumerate(source):
            if type(volume) is list:
                if len(volume) != len(source):
                    raise Exception(
                        "\n**ERROR** transfer_liquid_height given lists to but len(volume)!=len(source) %d %d" % (
                        len(volume), len(source)))
                Osource += [optimize_source_height(volume[j], well, offset=-1 * source_level_safety)[1]]
            else:
                Osource += [optimize_source_height(volume, well, offset=-1 * source_level_safety)[1]]
    else:
        if hasattr(volume, '__len__'):
            _, Osource = optimize_source_height(sum(volume), source, offset=-1 * source_level_safety)
        else:
            _, Osource = optimize_source_height(volume, source, offset=-1 * source_level_safety)
    if type(source) is tuple  or type(destination) is list:
        Odestination = []
        for j, well in enumerate(destination):
            if type(volume) is list:
                if len(volume) != len(destination): raise Exception(
                    "\n**ERROR** transfer_liquid_height given lists to but len(volume)!=len(destination) %d %d" % (
                    len(volume), len(destination)))
                Odestination += [optimize_destination_height(volume[j], well)[1]]
            else:
                Odestination += [optimize_destination_height(volume, well)[1]]
    else:
        if hasattr(volume, '__len__'):
            _, Odestination = optimize_destination_height(sum(volume), destination)
        else:
            _, Odestination = optimize_destination_height(volume, destination)
    return volume, Osource, Odestination

def change_pipette_speed(pipettes, aspirate_speed=None, dispense_speed=None, default=False):
    """
    change the speed a pipette aspirates and/or dispenses liquids
    :param pipette: list of pipettes
    :param default: used to set values back to default
    :param aspirate_speed:
    :param dispense_speed:
    :return:
    """
    for pipette in pipettes:
        aux['protocol_context'].comment("")
        aux['protocol_context'].comment(("Flow rates of {} are being changed").format(pipette.name))
        if default:
            aux['protocol_context'].comment("back to default")
            aux['protocol_context'].comment("")
            if pipette.name == 'p300_single':
                pipette.flow_rate.aspirate = 150
                pipette.flow_rate.dispense = 300
            elif pipette.name == 'p50_single':
                pipette.flow_rate.aspirate = 25
                pipette.flow_rate.dispense = 50
            elif pipette.name == 'p10_single':
                pipette.flow_rate.aspirate = 5
                pipette.flow_rate.dispense = 10
            elif pipette.name == 'p20_single_gen2':
                pipette.flow_rate.aspirate = 3.78
                pipette.flow_rate.dispense = 3.78
            elif pipette.name == 'p300_single_gen2':
                pipette.flow_rate.aspirate = 46.43
                pipette.flow_rate.dispense = 46.43
        else:
            if pipette.name == 'p300_single':
                aux['protocol_context'].comment("to aspirate= %g and dispense= %g" % (aspirate_speed, dispense_speed))
                aux['protocol_context'].comment("")
                pipette.flow_rate.aspirate = aspirate_speed
                pipette.flow_rate.dispense = dispense_speed
            elif pipette.name == 'p50_single':
                aux['protocol_context'].comment("to aspirate= %g and dispense= %g" % (aspirate_speed, dispense_speed))
                aux['protocol_context'].comment("")
                pipette.flow_rate.aspirate = aspirate_speed
                pipette.flow_rate.dispense = dispense_speed
            elif pipette.name == 'p10_single':
                aux['protocol_context'].comment("to aspirate= %g and dispense= %g" % (aspirate_speed/2, dispense_speed/2))
                aux['protocol_context'].comment("")
                pipette.flow_rate.aspirate = aspirate_speed/2
                pipette.flow_rate.dispense = dispense_speed/2
            elif pipette.name == 'p20_single_gen2':
                aux['protocol_context'].comment("to aspirate= %g and dispense= %g" % (aspirate_speed/2, dispense_speed/2))
                aux['protocol_context'].comment("")
                pipette.flow_rate.aspirate = aspirate_speed/2
                pipette.flow_rate.dispense = dispense_speed/2
            elif pipette.name == 'p300_single_gen2':
                aux['protocol_context'].comment("to aspirate= %g and dispense= %g" % (aspirate_speed, dispense_speed))
                aux['protocol_context'].comment("")
                pipette.flow_rate.aspirate = aspirate_speed
                pipette.flow_rate.dispense = dispense_speed


def _transfer(pipette, volume, source, destination, air_gap_before=False, air_gap=0, mix_before=False, mix_after=False, mix_level_safety=0, blow_out=False, touch_tip_on_blow_out=True, rate=1, source_level_safety=0, touch_tip=False, touch_tip_after_aspiration=False, **kwargs):
    '''
    volume,source,destination must not be lists at this stage - use optimized_transfer instead of this function.
    A tip must already be loaded at this stage!!
    air_gap_before = True will get almost the maximum available
    touch_tip_on_blow_out can be a negative number specifying how deep from top of well you wish to touch
    here blow_out can only be True or a specific Well object (not well.top or similar)
    source_level_safety should be a positive number that will go source_level_safety below the liquid level in the source to make sure no air is aspired
    touch_tip_after_aspiration splits up the transfer into separate aspiration and dispensing speeds. This is needed
    to handle very viscous solutions. With this option it is possible to very accurately control the pipette, i.e.
    moving speed, aspiration speed etc. of the pipette. It will also touch tip after aspiration at the source well. This
    is helpful to remove droplets remaining on the outside of the tip.
    '''
    if type(blow_out) is bool and blow_out == True:
        bl = True
    else:
        bl = False
    # check if air gap before is needed - if so one needs to blow out at some point
    airV = airgap_before(pipette, source, air_gap_before, volume) # does nothing if air_gap_before is None or False, and giving volume will print errors if any
    if airV > 0:
        tr_blow_out = destination
    else:
        tr_blow_out = False
    # mix stock a bit before starting to distribute
    if mix_before or type(mix_before) is tuple:
        if type(mix_before) is tuple:
            reps, vol = mix_before
        else:
            if type(mix_before) is int:
                reps = mix_before
            else:
                reps = 3
            vol = volume - pipette.min_volume  # mix a bit less of what you are going to aspirate at the end
            if vol < pipette.min_volume:
                vol = pipette.min_volume
        optimized_mix(pipette, repetitions=reps, volume=vol, locations=source, new_tip='never', touch_tip=False, air_gap_before=False, level_safety=mix_level_safety, blow_out=source)  # touch_tip False as we anyway dip down to aspirate in here
        airV = airgap_before(pipette, source, air_gap_before, volume)  # call again as we blew out above
    # the below will update filled volumes in source and destination
    # NOTE WELL - for the way the API is written with air_gap_before then it will blow out in trash even if below blow_out=False that's why we have tr_blow_out
    if touch_tip_after_aspiration: #Splits up the transfer in aspirate and dispense to enable a touch_tip after aspiration

        # DEFAULTspeeds = dict(list(map(lambda x: (x[0].lower(), x[1]),robot.config.default_max_speed.items())))  # dict {'x': 600, 'y': 400, 'z': 125, 'a': 125, 'b': 40, 'c': 40} in mm/sec
        max_speed_per_axis = {'x': 600, 'y': 400, 'z': 5, 'a': 125, 'b': 50, 'c': 50}  # MAKE Z much slower to dive in

        pipette.move_to(source.top(1))  # move 1 mm above top of well at default speed
        # robot.head_speed(x=100, y=80, z=5, a=5)
        aux['protocol_context'].max_speeds['X'] = 100
        aux['protocol_context'].max_speeds['Y'] = 80
        aux['protocol_context'].max_speeds['Z'] = 5
        aux['protocol_context'].max_speeds['A'] = 5

        if pipette.name is 'p10_single': # in order to maximise the air gap for the p10, it will aspirate as much as possible at this step
            pipette.aspirate(pipette.max_volume-volume-air_gap_before)

        pipette.aspirate(*optimize_source_height(volume, source))
        # pipette.delay(seconds=1) #pipette stays inside the solution; can help if the solution is very viscous and liquid movement lags behind the aspiration
        aux['protocol_context'].delay(seconds=2)
        optimized_touch(pipette, source, touch_tip=-7, blow_out=False)
        optimized_touch(pipette, source, touch_tip=-3, blow_out=False) #touch tip on insides of the well/tube twice to get rid of any droplets on the outside of the tip

        pipette.move_to(source.top(1))
        if pipette.name is not 'p10_single': #additional airgap to avoid viscous solutions dropping out of the tip during transfer; not recommended for small pipette because this normally doesn't happen and all the volume can then be used for blow out afterwards (as it was aspirated just 8 lines above)
            pipette.aspirate(air_gap)

        # robot.head_speed(combined_speed=max(DEFAULTspeeds.values()), **DEFAULTspeeds)  # restore default speed
        del aux['protocol_context'].max_speeds['Z']
        del aux['protocol_context'].max_speeds['A']
        # robot.head_speed(x=100, y=80)

        pipette.move_to(destination.top(1))
        # robot.head_speed(z=5, a=5)
        aux['protocol_context'].max_speeds['Z'] = 5
        aux['protocol_context'].max_speeds['A'] = 5

        pipette.dispense(*optimize_destination_height(volume, destination))
        # pipette.delay(seconds=1)  # pipette stays inside the solution; can help if the solution is very viscous and liquid movement lags behind the aspiration
        aux['protocol_context'].delay(seconds=2)
        optimized_touch(pipette, destination, touch_tip=-1, blow_out=True)
        pipette.move_to(destination.top(1))
        # robot.head_speed(combined_speed=max(DEFAULTspeeds.values()), **DEFAULTspeeds)  # restore default speed
        del aux['protocol_context'].max_speeds['X']
        del aux['protocol_context'].max_speeds['Y']
        del aux['protocol_context'].max_speeds['Z']
        del aux['protocol_context'].max_speeds['A']

        if type(mix_after) is tuple or mix_after > 0:
            mix_rate = 1
            if type(mix_after) is tuple:
                if len(mix_after) == 2:
                    reps, vol = mix_after
                elif len(mix_after) == 3:
                    reps, vol, mix_rate = mix_after
                elif type(mix_after) is int:
                    reps, vol = mix_after, volume
            else:
                reps, vol = 3, volume
            optimized_mix(pipette, repetitions=reps, volume=vol, locations=destination, new_tip='never', touch_tip=touch_tip_on_blow_out, level_safety=mix_level_safety, air_gap_before=air_gap_before, rate=mix_rate, blow_out=blow_out, **kwargs)

    else:
        # pipette.transfer(*transfer_liquid_height(volume, source, destination, source_level_safety=source_level_safety), air_gap=air_gap, touch_tip=touch_tip, new_tip='never', rate=rate, blow_out=tr_blow_out)  #When air gap or something else in pipette blow_out=False will go to the trash to blow out the same way as blow_out=True or None

        if volume > pipette.max_volume:
            raise Exception('volume greater than pipette volume %g >%g' %(volume, pipette.max_volume))

        pipette.aspirate(*optimize_source_height(volume, source, offset=-1 * source_level_safety), rate=rate) # Due to an AP2  bug it is necessary to split the transfer into aspirate and dispense, otherwise it will blow out in trash.
        pipette.dispense(*optimize_destination_height(volume, destination), rate=rate)
        if tr_blow_out!=False:
            pipette.blow_out(tr_blow_out)

        if type(mix_after) is tuple or mix_after > 0:
            mix_rate = 1
            if type(mix_after) is tuple:
                if len(mix_after) == 2:
                    reps,vol = mix_after
                elif len(mix_after) == 3:
                    reps, vol, mix_rate = mix_after
                elif type(mix_after) is int:
                    reps, vol = mix_after, volume
            else:
                reps, vol = 3, volume

            optimized_mix(pipette, repetitions=reps, volume=vol, locations=destination, new_tip='never', touch_tip=touch_tip_on_blow_out, level_safety=mix_level_safety, air_gap_before=air_gap_before, rate=mix_rate, blow_out=blow_out, **kwargs)
        elif isinstance(blow_out, Well):  # blow out here (otherwise by default it blows out in destination as from line below).
            optimized_touch(pipette, blow_out, touch_tip_on_blow_out, blow_out=True, speed=40, radius=0.98)  # will only blow out if touch_tip_on_blow_out is false
        else:
            optimized_touch(pipette, destination, touch_tip_on_blow_out, blow_out=bl, speed=40, radius=0.98)  # will touch if touch_tip_on_blow_out is not false, otherwise only blow_out or nothing at all if bl is False
    return


def optimized_transfer(pipette, volume, source, destination, air_gap_before=False, mix_before=False, mix_after=False, mix_level_safety=0, blow_out=False, touch_tip_on_blow_out=False, rate=1, new_tip='once', source_level_safety=0, **kwargs):
    '''
NOTE WELL - for the way the API is written if air_gap_before then it will blow out in trash when _transfer is called
    air_gap_before True will get almost the maximum available
    touch_tip_on_blow_out can be a negtive number specifying how deep from top of well you wish to touch
     this is only used when blow_out is a well
    source_level_safety should be a positive number that will go source_level_safety below the liquid level in the source to make sure no air is aspired
    '''
    if type(source) is tuple or type(source) is list:
        raise Exception(
            "\n**ERROR** optimized_transfer given list of source len(source) %d but this is not an implemented input - please just write a for loop on your sources and call it once per source well." % (
                len(source)))

    if type(source) is tuple  or type(destination) is list:
        if type(volume) is int or type(volume) is float or not hasattr(volume, '__len__'):  # convert to list
            volume = [volume] * len(destination)
        elif len(volume) != len(destination):
            raise Exception(
                "\n**ERROR** optimized_transfer given lists volume/destination but len(volume)!=len(destination) %d %d" % (
                len(volume), len(destination)))
        if new_tip == 'once' and not pipette.hw_pipette[
            'has_tip']: pipette.pick_up_tip()  # current_tip -> hw_pipette['has_tip'] in PAI2
        for j, dest in enumerate(destination):
            if new_tip == 'always' and not pipette.hw_pipette['has_tip']: pipette.pick_up_tip()
            _transfer(pipette, volume[j], source, dest, blow_out=blow_out, air_gap_before=air_gap_before, mix_before=mix_before, mix_after=mix_after, mix_level_safety=mix_level_safety, touch_tip_on_blow_out=touch_tip_on_blow_out, rate=rate, source_level_safety=source_level_safety, **kwargs)
            if new_tip == 'always': pipette.drop_tip()
        if new_tip == 'once': pipette.drop_tip()
    else:
        if new_tip in ['always', 'once'] and not pipette.hw_pipette['has_tip']: pipette.pick_up_tip()
        _transfer(pipette, volume, source, destination, blow_out=blow_out, air_gap_before=air_gap_before, mix_before=mix_before, mix_after=mix_after, mix_level_safety=mix_level_safety, touch_tip_on_blow_out=touch_tip_on_blow_out, rate=rate, source_level_safety=source_level_safety, **kwargs)
        if new_tip in ['always', 'once']: pipette.drop_tip()
    return


def _distribute(pipette, volume, source, destination, disposal_vol=None, air_gap_before=False, air_gap=0, mix_before=False, mix_after=False, touch_tip_mix=False, blow_out=False, rate=1, touch_tip_on_blow_out=True, mix_level_safety=0, source_level_safety=0, touch_tip=False, touch_tip_after_aspiration=False):
    '''
    use the function optimize_distribute, this is an auxiliary function of that
    at this stage the total volume must not exceed the max pipette volume - i.e. only one pipette aspirate should be done!
    A tip must already be loaded at this stage!!
    mix_before should typically called independently before using optimize_distribute - here is provided in case once wishes to pipette for every new tip (cold liquids)
    similarly for mix_after as distribute will usually go somewhere else to blow_out the disposal_vol,
    air_gap_before True will get almost the maximum available
    touch_tip_on_blow_out can be a negative number specifying how deep from top of well you wish to touch
    here blow_out can only be True or a specific Well object (not well.top or similar)
    source_level_safety should be a positive number that will go source_level_safety below the liquid level in the source to make sure no air is aspired
    '''
    if not hasattr(volume, '__len__') and hasattr(destination, '__len__'): volume = [volume] * len(destination)
    if hasattr(destination, '__len__') and len(destination) == 0:
        print_warning("**ERROR** _distribute() called with empty destination list! volume=%s source=%s" % (str(volume), str(source)))
        return
    if disposal_vol is None: disposal_vol = pipette.min_volume
    if pipette.max_volume < sum(volume) + disposal_vol:
        if pipette.max_volume >= sum(volume):
            disposal_vol = positive(pipette.max_volume - sum(volume))
        else:
            raise Exception("\nin _distribute from optimize_distrubute total volume to aspirate is %g uL from %s but pipette max is %g\n" % (sum(volume), str(volume), pipette.max_volume))
    opt_disposal_vol = disposal_vol  # by default we loose this volume unless we take it back with blow out there
    if type(blow_out) is bool and blow_out == True:
        bl = True  # will blow out in the trash
    else:
        bl = False
        if hasattr(source, 'point'):
            source_path = source.labware  # API2 coordinates given as input as opposed to just well
        else:
            source_path = source
        if hasattr(blow_out, 'point') and hasattr(blow_out, 'labware'):
            if blow_out.labware == source_path:
                opt_disposal_vol = 0  # we bring it back in original container
            # bl=blow_out
        elif isinstance(blow_out, Well):
            if blow_out == source_path:
                opt_disposal_vol = 0  # we bring it back in original container
            # bl=blow_out
    # check if air gap before is needed
    airV = airgap_before(pipette, source, air_gap_before, volume=sum(volume) + disposal_vol)  # does nothing if air_gap_before is None or False, and giving volume will print errors if any
    if airV > 0: aux['protocol_context'].comment("   _distribute: airGapVolume= %g uL volume = %s uL disposal_vol= %g uL\n" % (airV, str(volume), disposal_vol))
    # mix stock a bit before starting to distribute
    if mix_before == True or type(mix_before) is tuple:
        if type(mix_before) is tuple:
            reps, vol = mix_before
        else:
            reps, vol = 3, sum(volume) - pipette.min_volume  # mix a bit less than what you are going to actually aspirate
            if vol < pipette.min_volume:
                vol = pipette.min_volume
        optimized_mix(pipette, repetitions=reps, volume=vol, locations=source, new_tip='never', touch_tip=False, air_gap_before=False, blow_out=False, level_safety=mix_level_safety)  # touch_tip False as we anyway dip down to aspirate in here
        # airV= airgap_before(pipette, source, air_gap_before, sum(volume)+disposal_vol) # call again as we blew out above for mixing - don't do this for distribute as thanks to the disposal volume the distribute should be accurate even if something remains from the mixing
    # the below will update filled volumes in source and destination
    try:
        # print("DEB: bl=%s destinationNwells=%d disposal_vol=%g volume=%s" % (repr(bl),len(destination),opt_disposal_vol,repr(volume)))
        # ONE DISTIBUTE COMMAND SHOULD WORK BUT https://github.com/Opentrons/opentrons/issues/5170 BUG IN API.2 ABOUT ALWAYS BLOWING OUT IN TRASH (distribute command is left below commented out)
        vol, Z = optimize_source_height(sum(volume) + opt_disposal_vol, source, offset=-1 * source_level_safety)
        pipette.aspirate(vol + disposal_vol, Z)
        for j, dw in enumerate(destination):
            pipette.dispense(*optimize_destination_height(volume[j], dw, offset=0))
        # pipette.distribute(  *distribute_liquid_height(volume,source, destination, disposal_vol=opt_disposal_vol, source_level_safety=source_level_safety), disposal_vol=disposal_vol,rate=rate , new_tip='never' , blow_out=bl ) #Distribute should blow out in the trash if bl is true
    except Exception as err:
        raise Exception(
            "\n**EXCEPTION** occurred in distribute while trying volume %s uL,source %s, destination %s disposal_vol %s, blow_out %s \n%s\n" % (
            str(volume), str(source), str(destination), str(disposal_vol), str(blow_out), str(err)))
    if isinstance(blow_out, Well) or isinstance(blow_out, Location) or (
            type(blow_out) is tuple and len(blow_out) == 2 and isinstance(blow_out[0],
                                                                          Well)):  # blow out here (otherwise by default it blows out in trash at previous step - after or is coordinates
        # note that it doesn't make sense to touch a destination well in distribute, unless one we are blowing out in it
        optimized_touch(pipette, blow_out, touch_tip_on_blow_out, blow_out=True, speed=40, radius=0.98)  # will only blow out if touch_tip_on_blow_out is false
    if bl: pipette.blow_out()
    if type(mix_after) is tuple or mix_after > 0:
        mix_rate = 1
        if type(mix_after) is tuple:
            if len(mix_after) == 2:
                reps, vol = mix_after
            elif len(mix_after) == 3:
                reps, vol, mix_rate = mix_after
            elif type(mix_after) is int:
                reps, vol = mix_after, volume
        else:
            reps, vol = 3, volume
        optimized_mix(pipette, repetitions=reps, volume=vol, locations=destination[::-1], new_tip='never', touch_tip=touch_tip_mix, air_gap_before=air_gap_before, rate=mix_rate, blow_out=True, level_safety=mix_level_safety)
    return


def optimize_distribute(pipette, volume, source, destination, air_gap_before=False, air_gap=0, mix_before=False, mix_after=False, mix_level_safety=0, touch_tip_mix=False, blow_out=False, disposal_vol=None, touch_tip_on_blow_out=True, rate=1, source_level_safety=0, new_tip='once', touch_tip_after_aspiration=False, **kwargs):
    '''
    Volumes from the same source well are combined within the same tip, so that one aspirate can provide for multiple dispenses.
    air_gap_before True will get almost the maximum available
    touch_tip_on_blow_out can be a negtive number specifying how deep from top of well you wish to touch
     this is only used when blow_out is a well
      if touch_tip_after_aspiration is True no distribute will be done. Instead the volumes will be transferred using the
     _transfer function
    '''
    if type(source) is tuple  or type(source) is list:
        raise Exception("\n**ERROR** optimize_distribute given list of source len(source) %d but this is not yet an implemented input - please just write a for loop on your sources and call it once per source well." % (len(source)))
    if disposal_vol is None: disposal_vol = pipette.min_volume
    if type(source) is tuple or type(destination) is list:
        if type(volume) is int or type(volume) is float or not hasattr(volume, '__len__'): # convert to list
            volume = [volume] * len(destination)
        elif len(volume) != len(destination):
            raise Exception("\n**ERROR** optimized_transfer given lists volume/destination but len(volume)!=len(destination) %d %d" % (len(volume), len(destination)))
        # if isinstance(destination, WellSeries): destination = list(destination)
        if (new_tip == 'once' or type(new_tip) is int) and not pipette.hw_pipette['has_tip']: pipette.pick_up_tip()
        remove = [] # these will be those with 0 volume, which will be removed from both volume and destination
        for j, vol in enumerate(volume):
            if vol == 0: remove += [j]
        for j in sorted(remove, reverse=True):
            del volume[j], destination[j]
        tot_v, lastj = 0, 0
        n = 0
        for j, vol in enumerate(volume):
            if vol < 0:
                raise Exception("\n**ERROR** in optimize_distribute volume<0 (volume=%g uL at index %d of list %s for distribute)" % (vol, j, str(volume)))
            # split into gropus of max volume
            if vol + disposal_vol > pipette.max_volume: # raise error but this should not happen
                if pipette.max_volume - vol < 0: raise ValueError("in optimize_distribute given single volume of %g uL with a disposal volume of %g uL - together these are lareger than the pipette maximum volume of %g uL, not supported by optimize_distribute\n" % (vol, disposal_vol, pipette.max_volume))
                disposal_vol = pipette.max_volume - vol # not recommended
                #_transfer(pipette, vol,source, destination[j]  , blow_out=blow_out,air_gap_before=air_gap_before,mix_before=mix_before,mix_after=mix_after,touch_tip_on_blow_out=touch_tip_on_blow_out,rate=rate )
            tot_v += vol
            # aux['protocol_context'].comment ('HERE2 j %d tot_v %lf'%(j,tot_v))
            if tot_v + air_gap_before + air_gap + disposal_vol > pipette.max_volume: # transfer those before
                if (type(new_tip) is int and n > 0 and n % new_tip == 0):
                    pipette.drop_tip()
                    pipette.pick_up_tip()
                elif new_tip == 'always':
                    pipette.pick_up_tip()
                # the below will update the filled volume
                # aux['protocol_context'].comment ('HERE volume[lastj:j]= %s j %d lastj %d tot_v=%lf len(volume)=%d' % (str(volume[lastj:j]),j,lastj,tot_v,len(volume)))
                if j - lastj == 1: # just transfer!
                    if type(blow_out) is bool and blow_out == False:
                        bll = False
                    else:
                        bll = True
                    _transfer(pipette, volume[lastj:j][0], source, destination[lastj:j][0], blow_out=bll, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, rate=rate, touch_tip_on_blow_out=(touch_tip_on_blow_out or touch_tip_mix), touch_tip_after_aspiration=touch_tip_after_aspiration, **kwargs)
                elif len(destination[lastj:j]) > 0:
                    if touch_tip_after_aspiration:
                        for i, dest in enumerate(destination[lastj:j]):
                            if type(blow_out) is bool and blow_out == False:
                                bll = False
                            else:
                                bll = True
                            if i % 3 == 0 and i != 0:
                                pipette.drop_tip()
                                pipette.pick_up_tip()
                            _transfer(pipette, volume[lastj:j][i], source, dest, blow_out=bll, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, rate=rate, touch_tip_on_blow_out=(touch_tip_on_blow_out or touch_tip_mix), touch_tip_after_aspiration=touch_tip_after_aspiration, **kwargs)
                    else:
                        _distribute(pipette, volume[lastj:j], source, destination[lastj:j], disposal_vol=disposal_vol, blow_out=blow_out, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, touch_tip_mix=touch_tip_mix, touch_tip_on_blow_out=touch_tip_on_blow_out, rate=rate, source_level_safety=source_level_safety, mix_level_safety=mix_level_safety, **kwargs)
                lastj = j
                tot_v = vol # this j will be the lastj of next bit
                if new_tip == 'always': pipette.drop_tip()
                n += 1
        # do last bit
        if len(destination[lastj:j + 1]) > 0:
            if (type(new_tip) is int and n > 0 and n % new_tip == 0):
                pipette.drop_tip()
                pipette.pick_up_tip()
            elif new_tip == 'always':
                pipette.pick_up_tip()
            if touch_tip_after_aspiration:
                for i, dest in enumerate(destination[lastj:j + 1]):
                    if type(blow_out) is bool and blow_out == False:
                        bll = False
                    else:
                        bll = True
                    if i % 3 == 0 and i != 0:
                        pipette.drop_tip()
                        pipette.pick_up_tip()
                    _transfer(pipette, volume[lastj:j + 1][i], source, dest, blow_out=bll, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, rate=rate, touch_tip_on_blow_out=(touch_tip_on_blow_out or touch_tip_mix), touch_tip_after_aspiration=touch_tip_after_aspiration, **kwargs)
            else:
                _distribute(pipette, volume[lastj:j + 1], source, destination[lastj:j + 1], disposal_vol=disposal_vol, blow_out=blow_out, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, touch_tip_mix=touch_tip_mix, touch_tip_on_blow_out=touch_tip_on_blow_out, rate=rate, source_level_safety=source_level_safety, mix_level_safety=mix_level_safety, **kwargs)
        if (new_tip in ['always', 'once'] or type(new_tip) is int) and pipette.hw_pipette['has_tip']: pipette.drop_tip()
        n += 1
    else: # just transfer
        if (new_tip in ['always', 'once'] or type(new_tip) is int) and not pipette.hw_pipette['has_tip']: pipette.pick_up_tip()
        _transfer(pipette, volume, source, destination, blow_out=blow_out, air_gap_before=air_gap_before, air_gap=air_gap, mix_before=mix_before, mix_after=mix_after, rate=rate, touch_tip_on_blow_out=(touch_tip_on_blow_out or touch_tip_mix), touch_tip_after_aspiration=touch_tip_after_aspiration, **kwargs)
        if (new_tip in ['always', 'once'] or type(new_tip) is int) and pipette.hw_pipette['has_tip']: pipette.drop_tip()
    return


def optimize_transfer_two_pipettes(pipetteSmall, pipetteLarge, volume, source, destination, **kwargs):
    '''
    simply determines which pipette to use (given volume) to then call optimized_transfer()
    kwargs are those of optimized_transfer, e.g. : air_gap_before=True, mix_before=2, mix_after=False, blow_out=True, touch_tip_on_blow_out=True, new_tip='once'
    '''
    if pipetteSmall.max_volume > pipetteLarge.max_volume:
        place = pipetteSmall
        pipetteSmall = pipetteLarge
        pipetteLarge = place
        print_warning(
            "**WARNING** in optimize_distribute_two_pipettes pipetteSmall and pipetteLarge given in opposite order as pipetteSmall.max_volume>pipetteLarge.max_volume %g %g " % (
            pipetteSmall.max_volume, pipetteLarge.max_volume))
        # raise Exception("\n**ERROR** in optimize_distribute_two_pipettes pipetteSmall and pipetteLarge given in opposite order as pipetteSmall.max_volume>pipetteLarge.max_volume %g %g " % (pipetteSmall.max_volume,pipetteLarge.max_volume))
    if volume > pipetteSmall.max_volume:
        optimized_transfer(pipetteLarge, volume, source, destination, **kwargs)
    else:
        optimized_transfer(pipetteSmall, volume, source, destination, **kwargs)
    return

def optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, volume, source, destinations, new_tip='once', mode='speed', air_gap_before=False, air_gap=0, **kwargs):
    '''
    will fist separate the volumes according to the best pipette to transfer them
    see optimize_distribute for kwargs
    mode can be either 'precision' or 'speed', 'speed' is already very accurate, with Gen2 pipettes both modes should be equivalent
    '''
    if pipetteSmall.max_volume > pipetteLarge.max_volume:
        place = pipetteSmall
        pipetteSmall = pipetteLarge
        pipetteLarge = place
        print_warning("**WARNING** in optimize_distribute_two_pipettes pipetteSmall and pipetteLarge given in opposite order as pipetteSmall.max_volume>pipetteLarge.max_volume %g %g " % (pipetteSmall.max_volume, pipetteLarge.max_volume))
        # raise Exception("\n**ERROR** in optimize_distribute_two_pipettes pipetteSmall and pipetteLarge given in opposite order as pipetteSmall.max_volume>pipetteLarge.max_volume %g %g " % (pipetteSmall.max_volume,pipetteLarge.max_volume))
    if type(source) is tuple or type(source) is list:
        raise Exception("\n**ERROR** optimize_distribute_two_pipettes given list of source len(source) %d but this is not yet an implemented input - please just write a for loop on your sources and call it once per source well." % (len(source)))
    if type(volume) is int or type(volume) is float:  # just pick the best pipette - very easy one number
        if volume > pipetteLarge.min_volume: # should be faster, pick this one!
            optimize_distribute(pipetteLarge, volume, source, destinations, new_tip=new_tip, air_gap_before=air_gap_before, air_gap=air_gap, **kwargs)
        else:
            optimize_distribute(pipetteSmall, volume, source, destinations, new_tip=new_tip, air_gap_before=air_gap_before, air_gap=air_gap, **kwargs)
        return
    elif len(volume) != len(destinations):
        raise Exception("\n**ERROR** optimize_distribute_two_pipettes given lists volume/destination but len(volume)!=len(destination) %d %d" % (len(volume), len(destinations)))
    vlarge, vsmall = [], []
    dest_large, dest_small = [], []

    dec_vol = 0
    if air_gap is not None:
        dec_vol += air_gap
    if air_gap_before is not None:
        dec_vol += air_gap_before

    if mode == 'speed':
        for j, vol in enumerate(volume):
            if vol > pipetteLarge.min_volume:  # anything larger than min volume of pipetteLarge will go there to maximise speed
                vlarge += [vol]
                dest_large += [destinations[j]]
            else:
                vsmall += [vol]
                dest_small += [destinations[j]]
    elif mode == 'precision':
        for j, vol in enumerate(volume):
            if vol + dec_vol > pipetteSmall.max_volume:  # only what cannot fit in pipetteSmall will go in large to maximise precision
                vlarge += [vol]
                dest_large += [destinations[j]]
            else:
                vsmall += [vol]
                dest_small += [destinations[j]]
    if len(vlarge) > 0: optimize_distribute(pipetteLarge, vlarge, source, dest_large, new_tip=new_tip, air_gap_before=air_gap_before, air_gap=air_gap, **kwargs)
    if len(vsmall) > 0: optimize_distribute(pipetteSmall, vsmall, source, dest_small, new_tip=new_tip, air_gap_before=air_gap_before, air_gap=air_gap, **kwargs)
    return


def distribute_liquid_height(volume, source, destination, disposal_vol=0, source_level_safety=0):
    '''
    should ONLY be used when the total volume to distribute (sum(volume)) is
    smaller than the max volume of the pipette so that effectively there is only one aspirate operation
    Volumes from the same source well are combined within the same tip, so that one aspirate can provide for multiple
dispenses.
    return volume,source_optimisedZlevel,destination_optimisedZlevel
    so it can be used like e.g. :
    pipette.distribute( *distribute_liquid_height(30, plate.api1wells('A1'), plate.rows('2')) )
    Almost identical to optimize_transfer_height
     but treats differently the volume in the source well NOTE no pipette information are here so does not provide for maximum volume of pipette.
    source_level_safety should be a positive number that will go source_level_safety below the liquid level in the source to make sure no air is aspired
    '''
    tot_vol = 0
    if type(source) is tuple or type(destination) is list:
        Odestination = []
        for j, well in enumerate(destination):
            if type(volume) is list:
                if len(volume) != len(destination): raise Exception(
                    "\n**ERROR** distribute_liquid_height given lists to but len(volume)!=len(destination) %d %d" % (
                    len(volume), len(destination)))
                tot_vol += volume[j]
                Odestination += [optimize_destination_height(volume[j], well)[1]]
            else:
                tot_vol += volume
                Odestination += [optimize_destination_height(volume, well)[1]]
    else:  # should not be otherwise it does not really distribute
        if hasattr(volume, '__len__'):
            tot_vol += sum(volume)
            _, Odestination = optimize_destination_height(sum(volume), destination)
        else:
            tot_vol += volume
            _, Odestination = optimize_destination_height(volume, destination)
    if type(source) is tuple or type(
            source) is list:  # should not be in distribute - it would probably end up doing weird things
        Osource = [optimize_source_height(tot_vol + disposal_vol, well, offset=-1 * source_level_safety)[1] for j, well
                   in enumerate(source)]
    else:
        _, Osource = optimize_source_height(tot_vol + disposal_vol, source,
                                            offset=-1 * source_level_safety)  # tot_vol makes sure that the volume of all transfers is accounted for
    return volume, Osource, Odestination


def optimized_touch(pipette, well, touch_tip, blow_out=False, speed=40, radius=0.98):
    '''
    combineds touch and blow out, it is optimised so that it first goes down in the center of the well
     and then touches the edges. Using default touch_tip may make the pipette crash from above on the well edges
    if touch_tip is False or None it will only blow out or do nothing if  blow_out=False
    if touch tip is int or float it will be used as v_offset, which is The offset in mm from the top of the well to touch tip.
    should be negative unless you wish to touch above well (thus typically touching nothing)
    Default -1 mm default speed 60, here speed is reduced for safety.
    '''

    if type(well) is tuple and isinstance(well[0], Well): #API1
        well = well[0]  # coordinates - overwrites coordinates and touches at default height
    elif isinstance(well, Location): # API2
        well = well.labware
    if touch_tip is None or (type(touch_tip) is bool and touch_tip == False):
        if blow_out: pipette.blow_out(
            well.top())  # blow out at the tube/well top to avoid bubbles but also cross-contamination
        return
    if ('1.5ml-eppendorf' in repr(well) or 'eppendorf_1.5ml' in repr(well)) and props[well].properties['filled_volume'] < 450:
        pipette.move_to(well.top(-1))
        print_warning(
            "**Potential Warning** optimized_touch in eppendorf 1.5mL %s filled with volume<500uL %g may cause tip to slide on edge and possibly mess rack up (tried to move to top(-1) before touching tip)!\n" % (
            repr(well), props[well].properties['filled_volume']))
    if type(touch_tip) is int or type(touch_tip) is float:  # typically negative
        aux['protocol_context'].comment("touching tip at well %s touch_tip %s" % (str(well), str(touch_tip)))
        if blow_out: pipette.blow_out(well.top(touch_tip))  # blow out at the same height of touch tip
        pipette.touch_tip(v_offset=touch_tip, speed=speed,
                          radius=radius)  # v_offset is The offset in mm from the top of the well to touch tip. Default -1 mm default speed 60
    else:
        aux['protocol_context'].comment("touching tip at well %s default touch_tip %s" % (str(well), str(touch_tip)))
        if blow_out: pipette.blow_out(well.top(-1))  # 1mm below top of well is default of touch tip
        pipette.touch_tip(well, speed=speed, radius=radius)
    return


def airgap_before(pipette, well, air_gap_before, volume=None):
    '''
    loads a bit of air (air_gap_before uL) before actually loading the volume (which can be given as input to make sure it actually fits)
     this can be helpful for sticky/viscous solutions that may remain stuck to the oustide of the tip with blow_out
    '''
    if air_gap_before is not None and air_gap_before != False and air_gap_before > 0:
        if volume is not None and type(volume) is list: volume = sum(
            volume)  # probably missing disposal volume in this case
        if type(
                air_gap_before) is bool and air_gap_before == True and volume is not None:  # determine as almost max possible
            air_gap_before = positive(pipette.max_volume - volume)
            if air_gap_before > 2 * int(pipette.min_volume / 5. + 1): air_gap_before -= int(
                pipette.min_volume / 5. + 1)  # leave 1 uL in p10/p20, 2 in p50 and 7 in p300 as pistone space
        if volume is not None and volume + air_gap_before > pipette.max_volume:
            aux['protocol_context'].comment(
                '*Warn* in air_gap_before volume+air_gap_before > pipette.max_volume - respectively %g %g and %g setting air_gap_before to %g' % (
                volume, air_gap_before, pipette.max_volume, positive(pipette.max_volume - volume)))
            air_gap_before = positive(pipette.max_volume - volume)
        if isinstance(well, Location):  # already coordinates API2
            pipette.move_to(well[1].top(1))
        elif (type(well) is tuple and len(well) == 2 and isinstance(well[0], Well)):  # already coordinates API1
            pipette.move_to(well[0].top(1))  # for air gap we neglect input coordinates and still go outside top of well
        else:
            pipette.move_to(well.top(
                1))  # necessary for air_gap as usually this is after and location is predefined - 1mm above mixing well
        try:
            pipette.air_gap(
                volume=air_gap_before)  # get some air it should facilitate the blow out making sure all falls and does not remain on the tip
        except Exception:
            raise Exception(
                '*ERROR* in air_gap_before volume+air_gap_before > pipette.max_volume - respectively volume=%g air_gap_before=%g and max_volume= %g setting air_gap_before to %g' % (
                volume, air_gap_before, pipette.max_volume, positive(pipette.max_volume - volume)))
        return air_gap_before
    return 0


def optimized_mix(pipette, repetitions, volume, locations, level_safety=0, new_tip='always', touch_tip=True, air_gap_before=None, rate=1.0, blow_out=True, second_touch_on_blow_out=None):
    '''
    mixes volume in locations well, it exploits optimized liquid levels so to avoid dippind the tip too deep.
    # air_gap_before may facilitate successful blow out at end of mix, and touch_tip may help avoid cross-contaminations or volume
     transfer if using same tip
     level_safety should be a positive number that will go level_safety mm below optimized level to make sure the tip is actually in the samples
      (if negative it will be above level)
    if volume is None it automatically determines a good volume to use based on pipette.max_volume and filled volume of well
    '''
    if isinstance(locations, Well) or isinstance(locations, Location) or (
            type(locations) is tuple and len(locations) == 2 and isinstance(locations[0], Well)):  # single well
        locations = [locations]
    if new_tip == 'once': pipette.pick_up_tip()
    aux['protocol_context'].comment('Mixing..\n')
    for j, well in enumerate(locations):
        if isinstance(well, Location):  # API2
            location = well
            well = well.labware
        elif (type(well) is tuple and len(well) == 2 and isinstance(well[0], Well)):  # API1
            location = well
            well = well[0]
        else:
            location = well
        if volume is None:
            vol = min([int(props[well].properties['filled_volume'] / 2), int(
                0.85 * pipette.max_volume)])  # the smaller between 50% of well voume and 85% of max_volume of pipette
        elif type(volume) is list:
            vol = volume[j]
        else:
            vol = volume
        if vol > pipette.max_volume:
            print_warning(
                "**WARNING** in optimized_mix asked to mix a volume of %g uL but max pipette volume is %g uL, actually mixing %g uL" % (
                vol, pipette.max_volume, pipette.max_volume - pipette.min_volume))
            vol = pipette.max_volume - pipette.min_volume
        # check you have enough volume, else mix with less
        if 'filled_volume' in props[well].properties and props[well].properties['filled_volume'] <= vol:
            if props[well].properties['filled_volume'] <= pipette.min_volume:
                print_warning(
                    "**ERROR** in optimized_mix trying to mix %g uL in well %s but only %g uL are in well according to liquid tracking - NOT MIXING" % (
                    vol, repr(well), props[well].properties['filled_volume']))
                continue  # skip this well
            else:
                print_warning(
                    "**WARNING** in optimized_mix trying to mix %g uL in well %s but only %g uL are in well according to liquid tracking - actually mixing %g uL" % (
                    vol, repr(well), props[well].properties['filled_volume'],
                    max([int(props[well].properties['filled_volume'] / 2.), pipette.min_volume])))
                vol = max([int(props[well].properties['filled_volume'] / 2.), pipette.min_volume])
        if new_tip == 'always': pipette.pick_up_tip()
        airgap_before(pipette, well, air_gap_before, vol)  # does nothing if air_gap_before is None or False
        _, well_level = optimize_source_height(vol, location,
                                               offset=-1 * level_safety)  # this will artificially remove volume from 'filled_volume'
        pipette.mix(repetitions, vol, well_level, rate=rate)
        _, _ = optimize_destination_height(vol, location)  # this just restores volume to 'filled_volume'
        if second_touch_on_blow_out is not None:
            optimized_touch(pipette, well, touch_tip=second_touch_on_blow_out,blow_out=blow_out)  # if touch_tip False it only blows out
        optimized_touch(pipette, well, touch_tip=touch_tip, blow_out=blow_out)  # if touch_tip False it only blows out
        if new_tip == 'always': pipette.drop_tip()
    if new_tip == 'once': pipette.drop_tip()
    return


def optimized_serial_dilution(pipette, transfer_volume, locations, mix_repetitions=4, mix_volume='auto',
                              air_gap_before='auto', blow_out_after_mix=False, touch_tip=False, mix_in_source=True,
                              new_tip='once', trash_transfer_volume=False):
    '''
    ASSUMES all destinatino wells (locations[1:]) are already filled with the proper amount of buffer
    see do_serial_dilution if this is not the case.
    first well of lactions will be the starting well that begins the serial dilution, last the end well.
    Less volume will be left in start well and more in end well - all other shold have the volume unchanged.
    mix_volume='auto' sets it to 0.8*transfer_volume unless this plus the volume in locations[1] exceeds the 0.8*max_well volume
    '''
    if isinstance(locations, Well):  # single well
        print_warning("**ERROR** optimized_serial_dilution must have more than one location as input (given %s)" % (
            repr(locations)))
        return
    if transfer_volume + props[locations[1]].properties['filled_volume'] > 0.85 * props[locations[1]].properties[
        'total-liquid-volume']:  # if you want to transfer more than the well can hold (with a saftey of 0.8
        # if mix_volume=='auto' and trash_transfer_volume : # the idea would be to mix while always living some liquid in the pipette but it is quite hard to implement and also chance are C1 is already overflowing.
        #    mix_volume=int( 0.85*(locations[1].properties['total-liquid-volume']-locations[1].properties['filled_volume']) +0.5)
        print_warning(
            "**ERROR** optimized_serial_dilution trying to transfer more volume than destination %s can hold (with 0.8 saftey factor) - transfer_volume=%g volume already in destination is %g destination total capacity is %g (but actually using %g to allow space for tip)" % (
            repr(locations[1]), transfer_volume, props[locations[1]].properties['filled_volume'],
            props[locations[1]].properties['total-liquid-volume'], 0.8 * props[locations[1]].properties['total-liquid-volume']))
    if mix_volume == 'auto':
        mix_volume = int(0.8 * transfer_volume + 0.5)
    if air_gap_before == False:
        air_gap_before = None
    elif air_gap_before == 'auto':
        air_gap_before = int((pipette.max_volume - transfer_volume) / 2)
    aux['protocol_context'].comment('Starting serial dilution...')
    if mix_in_source:
        if new_tip in ['once', 'always']: pipette.pick_up_tip()
        optimized_mix(pipette, mix_repetitions, mix_volume, locations[0], new_tip='never', touch_tip=blow_out_after_mix,
                      air_gap_before=air_gap_before, blow_out=blow_out_after_mix)
        if new_tip == 'always': pipette.drop_tip()
    elif new_tip == 'once':
        pipette.pick_up_tip()
    for j, well in enumerate(locations):
        if j == len(locations) - 1: break  # last well reached - this is not a source
        if new_tip == 'always':
            pipette.pick_up_tip()
            airgap_before(pipette, well, air_gap_before=air_gap_before)
        optimized_transfer(pipette, transfer_volume, well, locations[j + 1], air_gap_before=False, mix_before=False,
                           mix_after=mix_repetitions, blow_out=False, touch_tip_on_blow_out=False, new_tip='never')
        # touch tip on source well before moving to destination - obviously do not blow out here in case the mixing set a bit off the volume.
        optimized_touch(pipette, well, touch_tip=touch_tip, blow_out=False)
        # get destination well and find its optimised level depending on the volume in there
        destination = locations[j + 1]
        _, dest_level = optimize_destination_height(transfer_volume,
                                                    destination)  # this will change the final stored volume, which is correct if not last in next cycle it will be used as a source and thus go down again
        pipette.dispense(transfer_volume, dest_level)
        pipette.mix(mix_repetitions, mix_volume, dest_level)
        # could be paranoid and add extra touch tip with blow out here
        if new_tip == 'always':
            optimized_touch(pipette, destination, touch_tip=touch_tip, blow_out=True)  # this time blow out
            if trash_transfer_volume: pipette.aspirate(*optimize_source_height(transfer_volume,
                                                                               destination))  # optimize_source_height will also edit the 'filled_volume' in this well
            pipette.drop_tip()
        elif blow_out_after_mix:  # no need to touch tip again as this same tip will be re-used.
            pipette.blow_out(destination.top(
                -1))  # blow out at the tube/well top to avoid bubbles but also cross-contamination this is to keep volume more precise - next cycle this will be a source well
    if new_tip != 'always':
        optimized_touch(pipette, destination, touch_tip=touch_tip, blow_out=False)  # we just blew out at end of loop
        if trash_transfer_volume: pipette.aspirate(*optimize_source_height(transfer_volume,
                                                                           destination))  # optimize_source_height will also edit the 'filled_volume' in this well
    if new_tip == 'once': pipette.drop_tip()
    return


def do_serial_dilution(pipetteSmall, pipetteLarge, transfer_volume, dilution_plate, dilution_plate_layout,
                       buffer_location, final_volume_per_well, number_of_concentrations, mix_repetitions=4,
                       trash_transfer_volume=False, add_zero=False):
    '''
    to do replicates just call this multiple times!
    Assumes the first concentration is already done and put in dilution_plate_layout - which is actually the first concentration plate layout which will be updated by this funciton with that of the whole dilution plate
     where keys are well names of dilution_plate
    and values are [ compound_name_str, compound_concentration_float ]
    return dilution_plate_layout - which is the same dictionary where keys are well names of dilution_plate and values are [ compound_name, concentration] updated with the newly filled wells where the dilution has been carried out
    transfer_volume is the uL to transfer across to do the serial dilution
    buffer_location is containers.Well object corresponding to where the buffer is (which is used to fill the wells where the dilution is then done)
    final_volume_per_well is the volume in each well (excluding first conentration that can also have more (it's not filled here)
    and last conentration which will also have transfer volume unless trash_transfer_volume is True
    '''
    # first put the buffer in the empty wells then do serial dilution
    nwells_needed = (number_of_concentrations - 1 + int(add_zero)) * len(
        dilution_plate_layout)  # we already did the first concentration at this stage
    free_wells_dilution = [w for w in dilution_plate.api1wells() if
                           'filled_volume' not in props[w].properties]  # now it will be less as we filled the first concentration
    dilution_wells = free_wells_dilution[:nwells_needed]  # will also add add_zero
    if len(free_wells_dilution) < nwells_needed:
        msg = "**ERROR** not enough free wells left in dilution_plate, left %d and needed %d, as requested %d concentration of %d compounds (first concentration already done)" % (
        len(free_wells_dilution), nwells_needed, number_of_concentrations, len(dilution_plate_layout))
        print_warning(msg)
        raise Exception(msg)

    optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, final_volume_per_well, buffer_location, dilution_wells,
                                     air_gap_before=True, mix_before=False, mix_after=False, blow_out=buffer_location,
                                     disposal_vol=None, touch_tip_on_blow_out=True,
                                     new_tip='once')  # blow out back in same buffer as wells are clean

    # do the dilutions and build the corresponding plate layout
    missing_concentrations = number_of_concentrations - 1 + int(add_zero)
    for j, well_coord in enumerate(list(
            dilution_plate_layout.keys())):  # at present it only contains the first concentration, we convert it into list for iterating so we can edit the dictionary in this loop without affecting the loop itself.
        locations = [dilution_plate.well(well_coord)] + dilution_wells[j * missing_concentrations: (
                                                                                                               j + 1) * missing_concentrations]  # allocate these wells for this dilution
        # back calculate concentrations from volumes, so these values can also be used as a control by comparing with the expected concentrations
        for i, w in enumerate(locations[:]):
            if i > 0:  # 0 is first concentration
                if add_zero and i == len(
                        locations) - 1:  # robot has already added it, but save it in dilution_plate_layout
                    locations.pop()
                    conc = 0
                else:
                    conc = transfer_volume * dilution_plate_layout[locations[i - 1].get_name()][1] / float(
                        transfer_volume + final_volume_per_well)
                if w.get_name() in dilution_plate_layout:
                    print_warning(
                        "**ERROR** overwriting location %s in dilution_plate_layout - this probably means we are going to do a dilution in an already used well!" % (
                            repr(w)))
                dilution_plate_layout[w.get_name()] = [dilution_plate_layout[locations[0].get_name()][0],
                                                       conc]  # compount name, concentration
        if transfer_volume > pipetteSmall.max_volume:
            optimized_serial_dilution(pipetteLarge, transfer_volume, locations, mix_repetitions=mix_repetitions,
                                      mix_volume='auto', touch_tip=-2, mix_in_source=True, new_tip='once',
                                      trash_transfer_volume=trash_transfer_volume)
        else:
            optimized_serial_dilution(pipetteSmall, transfer_volume, locations, mix_repetitions=mix_repetitions,
                                      mix_volume='auto', touch_tip=-2, mix_in_source=True, new_tip='once',
                                      trash_transfer_volume=trash_transfer_volume)
        if add_zero:  # robot has already added it, but save it in dilution_plate_layout
            for w in locations:
                if w.get_name() not in dilution_plate_layout:
                    dilution_plate_layout[w.get_name()] = []
    return dilution_plate_layout


def distributed_dilution(pipetteSmall, pipetteLarge, target_concentrations, dilution_plate, stocks_dictionary, buffer_location, final_volume_per_well, dilution_plate_layout={}, n_replicates=1, add_zero=False, add_fluorescent_compound_after_buffer_tuple=False, airgap_before_dilution_sample=False, airgap_after_dilution_sample=False, change_speeds=None, touch_tip_for_dilution=False, **kwargs):
    '''
    less accurate than do_serial_dilution but better at handling small and large volumes so that nothing overflows or exceeds the pipette max_volume
    target_concentrations can either be
     - a list with all target concentrations
     - a tuple with (first_concentration,  number_of_concentrations, dilution_factor) in this case first_concentration can be None to use the actual stock concentration from  stocks_dictionary as a first one.
        also in this case add_zero is used
    stocks_dictionary is a typical input dictionary of the method
     where keys are the name of the stocks to be used - one dilution will be done per each stock!
     and values are [location , concentraiton ] optionally also [, volume] but this is not used by this function
    return dilution_plate_layout - which is a dictionary that can also be given as input if needs updating,
      ACTUALLY tyring to use Well object as keys
      where keys are well names of dilution_plate and values are [ compound_name, concentration] updated with the newly filled wells where the dilution has been carried out
      if dilution_plate contains multiple plates ( multiplate will be set to True) keys become tuples like (slot,  well name)
    add_fluorescent_compound_after_buffer_tuple is a dirt fix to add a compound after the buffer but before the diluent - in this case a tuple must be given with (fluo_compound_source_location, flo_volume_to_add)
       and volume_to_add will be the same for all dilution wells. This trick may be useful when the stocks are aggregation prone.
    '''
    if pipetteSmall.max_volume > pipetteLarge.max_volume:
        place = pipetteSmall
        pipetteSmall = pipetteLarge
        pipetteLarge = place
        print_warning(
            "**WARNING** in distributed_dilution pipetteSmall and pipetteLarge given in opposite order as pipetteSmall.max_volume>pipetteLarge.max_volume %g %g " % (
            pipetteSmall.max_volume, pipetteLarge.max_volume))
    free_wells_dilution = [w for w in dilution_plate if 'filled_volume' not in props[w].properties]  # we may have already filled the first concentration
    multiplate, oldslot = False, None
    debug_holder = len(free_wells_dilution)
    for w in free_wells_dilution:
        if oldslot is not None and str(oldslot) != str(
                w.parent.parent):  # w._display_name.split()[-1] : # slot is w.parent.parent get_path =e.g. ['10', 'tempdeck', 'corning_96_wellplate_360ul_flat', 'A3']
            multiplate = True
            aux['protocol_context'].comment("Doing distributed_dilution across multiple target plates")
            break
        oldslot = w.display_name.split()[-1]

    if type(target_concentrations) is tuple:  # input with number of concentrations and diution factor
        target_concentrations_tup = target_concentrations  # save for all compounds in stocks_dictionary in this way
    else:
        target_concentrations_tup = None  # input with actual concentration values
    nw = 0
    for compound in stocks_dictionary:
        if target_concentrations_tup is not None:
            first_concentration, number_of_concentrations, dilution_factor = target_concentrations_tup
            if first_concentration is None: first_concentration = stocks_dictionary[compound][
                1]  # set to maximum available, which is that of the stock
            target_concentrations = [first_concentration]  # build recoursevely
            for j in range(number_of_concentrations - 1): target_concentrations += [
                target_concentrations[-1] * dilution_factor / (dilution_factor + 1)]
            if add_zero: target_concentrations += [0]
        if None in target_concentrations:
            target_concentrations[target_concentrations.index(None)] = stocks_dictionary[compound][
                1]  # set to maximum available, which is that of the stock
        aux['protocol_context'].comment("")  # empty line
        aux['protocol_context'].comment(
            "--> setting up distributed_dilution() of %s number_of_concentrations=%d n_replicates=%d final_volume_per_well=%g uL" % (
            compound, len(target_concentrations), n_replicates, final_volume_per_well))
        target_concentrations = sorted(target_concentrations)  # just in case, smaller to higher
        # calculate all volumes and encompass replicates
        compound_volumes = [n_replicates * [numpy.round(final_volume_per_well * c / stocks_dictionary[compound][1], 3)] for c
                            in
                            target_concentrations]  # this will be sorted from smallest volume as target_concentrations is sorted.

        compound_volumes = [item for sublist in compound_volumes for item in sublist]  # flatten

        buffer_volumes = [numpy.round(final_volume_per_well - cv, 3) for cv in compound_volumes]

        for i, volume in enumerate(buffer_volumes):
            if volume < 0.01:
                buffer_volumes[i] = 0
        aux['protocol_context'].comment("target_concentrations= %s" % (str(target_concentrations)))
        aux['protocol_context'].comment("     compound_volumes= %s" % (str(compound_volumes)))
        aux['protocol_context'].comment("     buffer_volumes  = %s" % (str(buffer_volumes)))
        # set destination wells and encompass replicates
        if nw + len(buffer_volumes) >= len(free_wells_dilution):
            msg = '**ERROR** in distributed_dilution too many wells needed to do %d replicates of %d concentrations of %d compounds - %d available wells in plate and latest index is %d debug_holder=%d' % (
            n_replicates, len(target_concentrations), len(stocks_dictionary), len(free_wells_dilution),
            nw + len(buffer_volumes), debug_holder)
            print_warning(msg)
            raise Exception(msg)  # must abort otherwise print cirpting exception when robot finishes plate
        destination_wells = free_wells_dilution[nw:nw + len(buffer_volumes)]
        nw += len(buffer_volumes)  # update for next compound
        # check for too small volumes, and fill dilution_plate_layout
        warned = False
        for j, v in enumerate(compound_volumes):
            # if multiplate : key=(destination_wells[j]._display_name.split()[-1],destination_wells[j].get_name()) # slot,well_name
            # else : key=destination_wells[j].get_name()
            key = destination_wells[j]
            if key in dilution_plate_layout:
                print_warning("**ERROR** in distributed_dilution for compound %s  overwriting dilution_plate_layout for well %s" % (compound, str(key)))
            dilution_plate_layout[key] = [compound, numpy.round(target_concentrations[int(j / n_replicates)], 2)]
            if v < pipetteSmall.min_volume and v > 0 and not warned:
                warned = True
                print_warning("**WARNING** in distributed_dilution for compound %s at least one compound_volume of %d uL corresponding to concentration %g is too small for pipettes - may cause inaccuracies in concentrations" % (compound, v, target_concentrations[int(j / n_replicates)]))
            elif buffer_volumes[j] < pipetteSmall.min_volume and buffer_volumes[j] > 0 and not warned:
                warned = True
                print_warning("**WARNING** in distributed_dilution for compound %s at least one buffer_volume of %d uL corresponding to concentration %g is too small for pipettes - may cause inaccuracies in concentrations - lowest accurate volume %g uL" % (compound, buffer_volumes[j], target_concentrations[int(j / n_replicates)], pipetteSmall.min_volume))
        aux['protocol_context'].comment("Doing distributed_dilution() of %s in %d wells" % (compound, len(destination_wells)))
        # distribute buffer first, as even for small volume it should not stick to the outside of the tip (while compound may so it is better to put it into the buffer).
        optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, buffer_volumes, buffer_location, destination_wells, new_tip='once', blow_out=buffer_location, touch_tip_on_blow_out=False)

        aux['protocol_context'].comment("")  # empty line
        aux['protocol_context'].comment("")  # empty line
        aux['protocol_context'].comment("**** DILUTION SAMPLE ****")
        aux['protocol_context'].comment("")  # empty line
        aux['protocol_context'].comment("")  # empty line

        if airgap_before_dilution_sample is not None: #enables air_gap before aspirating the dilution samples
            if type(airgap_before_dilution_sample)is int:
                air_gap_before = airgap_before_dilution_sample
            else:
                air_gap_before = True
        else:
            air_gap_before = False
        if airgap_after_dilution_sample is not None: #enables air_gap after aspirating the dilution samples
            if type(airgap_after_dilution_sample) is int:
                air_gap = airgap_after_dilution_sample
            else:
                air_gap = True
        else:
            air_gap = False
        if change_speeds is not None:
            change_pipette_speed(change_speeds[0], change_speeds[1], change_speeds[2])
        if touch_tip_for_dilution:
            touch_tip_on_blow_out=True
        else:
            touch_tip_on_blow_out=False

        if add_fluorescent_compound_after_buffer_tuple!=False and type(add_fluorescent_compound_after_buffer_tuple) is tuple:
            fluo_compound_source_location, flo_volume_to_add = add_fluorescent_compound_after_buffer_tuple
            optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, flo_volume_to_add, fluo_compound_source_location, destination_wells, new_tip=3, blow_out=fluo_compound_source_location, touch_tip_on_blow_out=False)
        # and then compounds, being sorted small to large volume it means it will dip in smaller concentrations and then move on to higher so no need to change tip
        optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, compound_volumes, stocks_dictionary[compound][0], destination_wells, new_tip=3, blow_out=stocks_dictionary[compound][0], touch_tip_on_blow_out=touch_tip_on_blow_out, air_gap_before=air_gap_before, air_gap=air_gap, **kwargs)

        if change_speeds is not None:
            change_pipette_speed(change_speeds[0], default=True)

    return dilution_plate_layout


def put_stocks_at_same_concentration(pipetteSmall, pipetteLarge, end_concentration, end_volume, compound_stocks,
                                     buffer_location, destination_container, destination_plate_layout={},
                                     min_volume_for_buffer_before=10):
    '''
    compound_stocks is a dictionary with compound_name as keys and [stock_location, stock_concentration, stock_vol] as values
     both stock_location in above dictionary and buffer_location should be well objects.
     stock_location can be within the destination_container as long as this has been initialised with 'filled_volume' in the properties of the
       relevant wells, otherwise it will be assumed to be fully empty!
    return destination_plate_layout a dictionary whose keys are well coordinates in destination_container (e.g. 'A1') and values are [compound_name, concentration]
    '''
    # set initial volume, check for possible errors and calculate the volumes to create the first concentration of the serial dilution
    dilution_vols = {}  # keys are prot names, values are [stock_location, needed_vol, buffer_volume ] and needed_vol+buffer_volume will give the desired first_concentration_of_dilution
    do_buffer_before = 0  # if it will be positive in most cases buffer_volume is greater than needed_vol and thus buffer will be put first, otherwise first antibody.
    for compound_name in compound_stocks:
        stock_location, initial_conc, stock_vol = compound_stocks[compound_name]
        if 'filled_volume' in props[stock_location].properties:
            if int(props[stock_location].properties['filled_volume'] + 0.5) != int(
                    stock_vol + 0.5) and destination_plate_layout == {}:  # you may have already filled this, but raise warning if different
                print_warning(
                    "**WARNING** in put_stocks_at_same_concentration stock_location.properties['filled_volume']=%g is different from stock_vol=%g declared in input for compound %s at location %s" % (
                    sprops[stock_location].properties['filled_volume'], stock_vol, compound_name, repr(stock_location)))
                props[stock_location].properties['filled_volume'] = stock_vol  # ovewrite
        else:
            props[stock_location].properties['filled_volume'] = stock_vol  # set initial volume in class
        if initial_conc < end_concentration:
            print_warning(
                "**ERROR** for compound %s (at %s) compound concentration of %g is too low to reach needed end_concentration %g - doublecheck antibody_percent_of_final_volume" % (
                compound_name, repr(stock_location), initial_conc, end_concentration))
        needed_vol = end_concentration * end_volume / float(initial_conc)
        buff_vol = end_volume - needed_vol
        if stock_vol < needed_vol:
            print_warning(
                "**ERROR** for compound %s (at %s) compound stock volume of %g uL is too low to reach needed end_concentration %g (would require=%g uL) - doublecheck antibody_percent_of_final_volume" % (
                compound_name, repr(stock_location), stock_vol, end_concentration, needed_vol))
        dilution_vols[compound_name] = [stock_location, int(needed_vol + 0.5),
                                        int(buff_vol + 0.5)]  # round to int (1uL accuracy in robot)
        if buff_vol > needed_vol:
            do_buffer_before += 1
        else:
            do_buffer_before -= 1

    # as stock of antibody may be in the plate (if not in eppendorf) get the free wells to avoid using already filled wells

    if type(destination_container) is not list:
        welllist = destination_container.api1wells()
    else:
        welllist = destination_container
    free_wells_dilution = [w for w in welllist if 'filled_volume' not in props[w].properties]

    # create the location of the first end_concentration of the serial dilution, start creating plate_layout and make list of volumes to exploit transfer functions
    first_concentration = {}  # keys are compound_name and values are the location (which includes the filled volume under properties['filled_volume'])
    # destination_plate_layout={} # keys are well coordinates (e.g. 'A1') and values are [compound_name, concentration]
    destination_wells_list, buff_volume_lists_first, buff_volume_list_later, destination_wells_list_buff_first, destination_wells_list_buff_later, stock_compound_well_list, stock_compound_volumes_list = [], [], [], [], [], [], []
    for j, compound_name in enumerate(dilution_vols):
        well = free_wells_dilution[j]
        destination_wells_list += [well]
        if dilution_vols[compound_name][2] >= min_volume_for_buffer_before:
            buff_volume_lists_first += [dilution_vols[compound_name][2]]
            destination_wells_list_buff_first += [well]
        else:
            buff_volume_list_later += [dilution_vols[compound_name][2]]
            destination_wells_list_buff_later += [well]
        stock_compound_well_list += [dilution_vols[compound_name][0]]
        stock_compound_volumes_list += [dilution_vols[compound_name][1]]
        if well.get_name() in destination_plate_layout:
            print_warning(
                "**ERROR** in put_stocks_at_same_concentration when filling destination_plate_layout overwriting %s\n" % (
                    well.get_name()))
        destination_plate_layout[well.get_name()] = [compound_name, end_concentration]
        first_concentration[compound_name] = well
    # actually engage robot to do the end_concentration
    # distribute buffer for those volumes that are larger than min_volume_for_buffer_before, transfer other after stock to maximise accuracy
    aux['protocol_context'].comment(
        'put_stocks_at_same_concentration distributing N=%d buffer volumes larger than %g uL' % (
        len(buff_volume_lists_first), min_volume_for_buffer_before))
    optimize_distribute_two_pipettes(pipetteSmall, pipetteLarge, buff_volume_lists_first, buffer_location,
                                     destination_wells_list_buff_first, air_gap_before=True, mix_before=False,
                                     mix_after=False, blow_out=buffer_location, disposal_vol=None,
                                     touch_tip_on_blow_out=True, new_tip='once')  # blow out back in same buffer

    # do compounds using transfer and not distribute (as you change tip anyway)
    aux['protocol_context'].comment(
        'put_stocks_at_same_concentration transferring %d compounds' % (len(stock_compound_volumes_list)))
    for j, v in enumerate(stock_compound_volumes_list):
        if v == 0: continue
        optimize_transfer_two_pipettes(pipetteSmall, pipetteLarge, v, stock_compound_well_list[j],
                                       destination_wells_list[j], new_tip='once', air_gap_before=False,
                                       mix_before=False, mix_after=False, blow_out=True, touch_tip_on_blow_out=True)

    # now transfer by changing tip remaining small buffer volumes if any
    if len(buff_volume_list_later) > 0:
        aux['protocol_context'].comment(
            'put_stocks_at_same_concentration transferring N=%d buffer volumes smaller than %g uL' % (
            len(buff_volume_list_later), min_volume_for_buffer_before))
        for j, v in enumerate(buff_volume_list_later):
            if v == 0: continue
            optimize_transfer_two_pipettes(pipetteSmall, pipetteLarge, v, buffer_location,
                                           destination_wells_list_buff_later[j], new_tip='once', air_gap_before=False,
                                           mix_before=False, mix_after=False, blow_out=True, touch_tip_on_blow_out=True)
    aux['protocol_context'].comment('')
    return destination_plate_layout


def test_labware(lab, pipette, additional_wells_to_test=[], touch_tip=False):
    '''
    test a piece of labware by moving the tip on the pipette to various locations (and pausing run to wait for user to resume at each)
    '''
    if type(additional_wells_to_test) is not list: additional_wells_to_test = [additional_wells_to_test]
    aux['protocol_context'].comment("TESTING labware %s" % (repr(lab)))
    new_tip = False
    if not pipette.hw_pipette['has_tip']:
        new_tip = True
        pipette.pick_up_tip()
    top_corners = [lab.rows(0)[0][0], lab.rows(0)[0][-1]]
    lower_corners = [lab.rows(-1)[0][0], lab.rows(-1)[0][-1]]
    num = lab.rows(0)[0][int(len(lab.rows(0)[0]) / 2)].get_name()[1:]
    let = lab.columns(0)[0][int(len(lab.columns(0)[0]) / 2)].get_name()[:1]
    central_well = [lab.well(let + num)]
    tested = []
    for well in top_corners + lower_corners + central_well + [lab.well(wc) if type(wc) is str or type(wc) is int else wc
                                                              for wc in additional_wells_to_test]:
        wn = well.get_name()
        if wn in tested: continue
        aux['protocol_context'].comment("Went to TOP of well %s" % (wn))
        pipette.move_to(well.top())
        aux['protocol_context'].pause(
            "Press RESUME to continue - it will move to bottom of well/tube, exactly %g mm below current position!" % (
            well.top()[1][2]))
        pipette.move_to(well.bottom())
        if touch_tip:
            aux['protocol_context'].pause("Press RESUME to touch tip on well edges at the top")
            optimized_touch(pipette, well, touch_tip, blow_out=False, speed=20, radius=0.99)
        tested += [wn]
    pipette.aspirate(pipette.min_volume, top_corners[0])  # needed otherwise labware is not seen as being used
    pipette.dispense(pipette.min_volume, top_corners[0])
    aux['protocol_context'].comment("Returning tip")
    if new_tip: pipette.return_tip()
    return tested


def print_plate_layout(platelayout, is384=None, name_index=0, concentration_index=None, volume_index=None,
                       plate_name='ASSAY PLATE', comments='', concentration_unit='uM', round_concentration=2,
                       volume_unit='uL', delimiter=',', print_also_python_dict=True):
    '''
    print to robot log a plate layout - the expectation is that the input dict platelayout
     has keys like 'A1' and values are list that may (or not if respective indices are set to None)
     contain [ compound_name , concentration,  volume_index]
    handles 96 well or 384 well plates if is384 is True
    '''
    if is384 in [None, 'auto']:
        auto384 = True
    else:
        auto384 = False
    multiplate, oldslot, slot_groups = False, None, {}
    for w in platelayout:  # only works properly with modern plate layouts, otherwise assigns '' as slot
        if type(w) is str:
            sl, nam = '', w
        else:
            sl, nam = w.display_name.split()[-1], props[w].get_name()
        if sl not in slot_groups: slot_groups[sl] = {}
        slot_groups[sl][nam] = platelayout[w]
        if oldslot is not None and oldslot != sl:
            if not multiplate: aux['protocol_context'].comment(
                "Printing print_96well_plate_layout across multiple plates")
            multiplate = True
        oldslot = sl

    for slot in slot_groups:
        if multiplate:
            aux['protocol_context'].comment("")
            aux['protocol_context'].comment("PLATE AT SLOT %s" % (slot))

        plate_layout = slot_groups[slot]
        if auto384:
            is384 = False
            for k in plate_layout:
                if k[0] in ['I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']:
                    is384 = True
                    break
                if k[-2:] in list(map(str, range(13, 25))):
                    is384 = True
                    break
        plate_layout_python = {}
        if type(concentration_unit) is not str: concentration_unit = ''
        if type(volume_unit) is not str: volume_unit = ''
        if is384:
            table_lines = [['plate_slot_%s' % (slot)] + list(map(str, range(1, 25)))]  # header first
        else:
            table_lines = [['plate_slot_%s' % (slot)] + list(map(str, range(1, 13)))]  # header first
        cis = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        if is384: cis += ['I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
        for a in cis:
            if is384:
                table_lines += [[a] + [''] * 24]  # initialise with all empty
            else:
                table_lines += [[a] + [''] * 12]  # initialise with all empty
        for well in sorted(plate_layout):  # note this is not an ordered dict
            plate_layout_python[well] = [plate_layout[well][j] for j in [name_index, concentration_index, volume_index]
                                         if j is not None]
            st = ''
            if name_index is not None: st += str(plate_layout[well][name_index])
            if concentration_index is not None and plate_layout[well][
                concentration_index] is not None: st += ' %g%s' % (
            round(plate_layout[well][concentration_index], round_concentration), concentration_unit)
            if volume_index is not None and plate_layout[well][volume_index] is not None: st += ' %g%s' % (
            plate_layout[well][volume_index], volume_unit)
            table_lines[cis.index(well[0]) + 1][int(well[1:])] = st

        aux['protocol_context'].comment('LAYOUT of %s %s' % (plate_name, comments))
        aux['protocol_context'].comment(
            '(copy and paste the lines below into Excel - you may need to use the Data/Text to Columns.. function in excel using comma and : as delimiter to separate the lines into different cells and finally have a decent layout):')
        for l in table_lines:
            aux['protocol_context'].comment(delimiter.join(l))

        if print_also_python_dict:
            aux['protocol_context'].comment('')  # new line
            l = '%s = dict([' % (
                plate_name.replace(' ', '_'))  # the robot does not print graphs and also single quotes '
            for k in plate_layout_python:
                if type(plate_layout_python[k]) is str:
                    l += '("%s", "%s"),' % (k, plate_layout_python[k])
                else:
                    l += '("%s", %s),' % (k, plate_layout_python[k])
            aux['protocol_context'].comment(
                'Python output plate layout slot %s in dictionary format, well to samples name - can copy and paste in python shell or script:' % (
                    slot))
            aux['protocol_context'].comment(l[:-1] + '])')
    return


def api1wells(self, *args, **kw):
    '''
    emulates API 1. with 'to=' kwargs
    if not present simply use API 2 version
    issue with API2 is that dictionaries are sorted from python 3.7 but opentrons is compatible
      with python 3.6 onwards, so cannot assume sorted dictionaries here.
    '''
    # print('DEB:',hasattr(self,'wells'),len(args),self)
    # print (dir(self))
    if 'to' in kw:
        end_well_included = kw['to']
        if type(end_well_included) is int: end_well_included = self.wells()[end_well_included]._display_name.split()[
            0]  # well name
        if end_well_included not in self.wells_by_name():
            raise Exception("**ERROR** in api1wells API1-adapted labware.wells: end well %s not in labware %s\n" % (
            str(end_well_included), repr(self)))
    else:
        return self.wells(*args, **kw)  # return normal API2 wells results

    if len(args) > 0:
        start_well = args[0]
        if type(start_well) is int: start_well = self.wells()[start_well]._display_name.split()[0]  # well name
        if start_well not in self.wells_by_name():
            raise Exception("**ERROR** in api1wells API1-adapted labware.wells: start well %s not in labware %s\n" % (
            str(start_well), repr(self)))
    else:
        start_well = 'A1'
    print('DEB: start_well=%s end_well_included=%s' % (start_well, end_well_included))
    well_list, addw = [], False
    for w in self.wells():
        if w.get_name() == start_well.upper():
            well_list += [w]
            addw = True
        elif w.get_name() == end_well_included.upper():
            well_list += [w]
            addw = False
            break
        elif addw:
            well_list += [w]
    return well_list

class New_object:
    pass

def makeAPI2wellsBackCompatible(mainprotocol):
    '''
    just add the properties dict to each well as it use to be there in API1 and we exploit it to track
    volumes and liquid levels in various containers.
    '''
    global props
    props = {}
    for slot in mainprotocol.loaded_labwares:
        if slot in [12, '12']:
            continue  # fixed trash
        l = mainprotocol.loaded_labwares[slot]
        # if not hasattr(mainprotocol.loaded_labwares[slot],'new_wells') :
        #    mainprotocol.loaded_labwares[slot].new_wells=mainprotocol.loaded_labwares[slot].wells # API 2 well function saved as new_wells
        # if 'tip rack' in repr(l).lower() : continue # don't change the tip racks
        if hasattr(l, 'wells'):
            # l.__getitem__=V1_getitem_for_iteration_on_labware
            for i, w in enumerate(mainprotocol.loaded_labwares[slot].wells()):
                # w.get_name=lambda : w.display_name.split()[0] # DOES not work always returns last well - must use  types.MethodType
                key = mainprotocol.loaded_labwares[slot].wells(i)[0]
                # print(type(key), key)
                props[key] = New_object()
                props[key].get_name = types.MethodType(lambda self: self.display_name.split()[0], w)

                if not hasattr(props[key], 'properties'):
                    props[key].properties = {}  # other custom properties such as 'filled_volume' or 'warned' are added by the protocol as it runs
                    props[key].properties['total-liquid-volume'] = w.max_volume
                    # depth = w.geometry.top() - w.geometry.bottom() # In case they depricate .depth again this can be used as well
                    props[key].properties['depth'] = w.depth
                    if w.diameter is not None:
                        props[key].properties['diameter'] = w.diameter
                        props[key].properties['shape'] = 'circular'
                    if w.length is not None:
                        props[key].properties['length'] = w.length
                        props[key].properties['shape'] = 'square'
                    if w.width is not None:
                        props[key].properties['width'] = w.width
                        props[key].properties['shape'] = 'square'

            if not hasattr(mainprotocol.loaded_labwares[slot], 'api1wells'):
                print('modifying Labware %s' % (repr(l)))
                mainprotocol.loaded_labwares[slot].api1wells = types.MethodType(api1wells,
                                                                                mainprotocol.loaded_labwares[slot])

    return  # mainprotocol


#######################################################################################
#
#   END OF FUNCTION LIBRARY - EDIT labware and samples BELOW, read all comments
#
#######################################################################################


metadata = {
    'apiLevel': '2.5',  # use new version
    'protocolName': 'Peg Assay',
    'author': 'Marc <mo428@cam.ac.uk>,Pietro <ps589@cam.ac.uk>',
    'description': 'Fills a plate with varying PEG concentration and adds appropriate amounts of buffer and protein.',
}


def run(protocol: protocol_api.ProtocolContext) : # for console protocol= protocol_api.ProtocolContext()
    aux['protocol_context']=protocol # set right ProtocolContext for function library

    #######################################################################################
    # SAMPLE AND RACK-SLOTS - EDIT FROM HERE:
    #######################################################################################
    # TIP location(s) - put tip box in SLOT - a tip in A1 may be needed for calibration
    tiprack_300 = [protocol.load_labware('opentrons_96_tiprack_300ul', '9')]#,labware.load('opentrons-tiprack-300ul', slot='11')]  # identical to other but better not to use only one rack for two pipettes
    tiprack_10 = [protocol.load_labware('opentrons_96_tiprack_20ul', '11'), protocol.load_labware('opentrons_96_tiprack_20ul', '4')]
    start_at_tip_10 = 'A1'
    start_at_tip_300 = 'A1'  # make sure that all tips are present from this location onwards moving FIRST downwards and then rightwards - a tip in A1 may be needed for calibration

    small_pipette = protocol.load_instrument('p20_single_gen2',mount='left', tip_racks=tiprack_10)
    large_pipette = protocol.load_instrument('p300_single_gen2',mount='right', tip_racks=tiprack_300)

    assay_plate = protocol.load_labware('greiner_384_wellplate_28ul', '5') #,labware.load('96-flat', slot='4'),labware.load('96-flat', slot='1') ]#, share=True) # share is important as otherwise it could not occupy the same location as the temperature module above

    plate_first_well = 'A4'  # in case you are adding samples to a previously used plate with empty wells - all needed wells FIRST downwards and THEN rightwards of this must be empty!
    plate_last_well = 'P24' # None to fill the whole plate, otherwise it fills top to down and then left to right up to plate_last_well included

    eppendorfs = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap','7')  # these may hold the antibody stocks REPLACE with 'opentrons_24_tuberack_eppendorf_2ml_safelock_snapcap' if using 2 mL eppendorfs or 'opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap' for 1.5mL
    eppendorfs2 = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5mL_safelock_snapcap', '8')
    falcons15 = protocol.load_labware('opentrons_10_tuberack_falcon_4x50ml_6x15ml_conical', '6')  # for the antibody/compound buffer which may or not be the same as the abeta buffer

    #######################################################################################
    # SAMPLE VOLUMES
    #######################################################################################

    # buffer of compounds_for_dilution:
    buffer_location = {
        # buffers needed for different PEGs. For each PEG variant, a buffer needs to be implemented. They also have to be in the same order. Buffer 0 will be used for the first PEG in compounds_for_dilution.
        # key: location, volume, pH
        0: (falcons15.well('A1'), 13000, 'pH 7'),
        1: (falcons15.well('A1'), 13000, 'pH 7'),
        2: (falcons15.well('A1'), 13000, 'pH 7')
    }

    # this compound will be diluted in the plate.
    compounds_for_dilution = {
    # this is the titrant (Location, Concentration, Volume uL)
        'PEG15': (eppendorfs2.well('A1'), 15, 1000),
        'PEG30': (eppendorfs2.well('B1'), 30, 1000),
        'PEG50': (eppendorfs2.well('C1'), 50, 1150)
    }
    concentration_unit = '%'  # only for printout purposes (concentration_unit of compounds_for_dilution)

    # the concentration of this will be constant in the plate
    samples = {
        # key: location, concentration, volume
        'HzATNP ': (eppendorfs.well('A1'), 3, 95)
    }

    # Determine the concentration of PEG that are needed in the assay. For each group maximum can be 2/3 of stock concentration. I.e. 10 for 15% stock
    PEG_Low_conc = numpy.asarray([0, 3, 5, 6, 7, 9])
    PEG_Mid_conc = numpy.asarray([11, 13, 15, 17, 20])
    PEG_High_conc = numpy.asarray([23, 26])

    target_concentrations = { # making a dict out of the concentrations in case more than one PEG variant is needed
        'PEG15': list(PEG_Low_conc),
        'PEG30': list(PEG_Mid_conc),
        'PEG50': list(PEG_High_conc)
    }

    final_protein_concentration = 1 # concentration of the compound(s) added after the dilution; standard is 3 mg/mL stock for a final concentration of 1 mg/mL

    n_replicates_per_concentration = 3 # number of replicates per concentration of the samples that is diluted. Including number of controls (e.g. n_controls + n_samples = n_replicates_per_concentration)
    leave_n_controls = 1  # number of replicates of the above n_replicates_per_concentration that should be used as control (to which control_buffer_location will be added instead of samples)

    final_volume_per_well = 10  # uL
    leave_edges_empty = False  # to safeguard from evaporation in particularly long assays some prefer to leave the wells in the edges empty, set to True if this is the case
    leave_corners_empty = True

    add_zero = False


    # #######################################################################################
    # #######################################################################################
    # #######################################################################################
    # #  PROTOCOL - usually NO NEED TO EDIT BELOW
    # #######################################################################################
    # #######################################################################################
    # #######################################################################################

    if 0 in target_concentrations: add_zero = False

    makeAPI2wellsBackCompatible(protocol)

    # these first few commands are necessary to set the stage for the robot, i.e. what is the first tip to use in each tiprack,
    # what is filled_volume of the buffers and compounds

    # set starting tip as from input
    if type(tiprack_10) is not list:
        small_pipette.starting_tip=tiprack_10.well(start_at_tip_10)
    else:
        small_pipette.starting_tip=tiprack_10[0].well(start_at_tip_10)  # assumes others will have A1
    if type(tiprack_300) is not list:
        large_pipette.starting_tip=tiprack_300.well(start_at_tip_300)
    else:
        large_pipette.starting_tip=tiprack_300[0].well(start_at_tip_300)  # assumes others will have A1

    for buffer in buffer_location:
        props[buffer_location[buffer][0]].properties['filled_volume'] = buffer_location[buffer][1]

    for comp in compounds_for_dilution:
        props[compounds_for_dilution[comp][0]].properties['filled_volume'] = compounds_for_dilution[comp][2]

    for comp in samples:
        props[samples[comp][0]].properties['filled_volume'] = samples[comp][2]

    if leave_n_controls >= n_replicates_per_concentration:
        msg = '**ERROR** leave_n_controls>=n_replicates_per_concentration respectively %d and %d means that only controls will be done!' % (
        leave_n_controls, n_replicates_per_concentration)
        raise Exception(msg)

    if len(buffer_location) != len(compounds_for_dilution): # this is so it is easier to programme. The buffer can be the same in the same location, but it needs to be in the relevant dict twice.
        msg = '**ERROR** there needs to be as many buffers as there are compounds for dilution'
        raise Exception(msg)

    if not hasattr(target_concentrations, 'keys'):  # convert to dictionary one per compounds_for_dilution
        dd = {}
        for dil in compounds_for_dilution: dd[dil] = target_concentrations
        target_concentrations = dd


    # calculate the compound volumes that will be added at the end, the dilution volume pre-compound
    proteins_volumes = {}
    dilution_volumes_pre_compound = {}
    adjusted_target_concentrations = {}  # target concentration of dilutent to have so that the final concentration (after adding compound) will match input target_concentrations
    for compF in samples:
        proteins_volumes[compF] = final_volume_per_well * final_protein_concentration / \
                                  samples[compF][1]
        dilution_volumes_pre_compound[compF] = final_volume_per_well - proteins_volumes[compF]
        adjusted_target_concentrations[compF] = {}
        for dil in target_concentrations:
            adjusted_target_concentrations[compF][dil] = [tc * final_volume_per_well / dilution_volumes_pre_compound[compF]
                                                          for tc in target_concentrations[dil] if tc is not None]
            if None in target_concentrations[dil]: adjusted_target_concentrations[compF][dil] += [
                None]  # will be set max concentration is stock concentration

    # determines which wells can be used in the assay plate, starting with the named first well. First all already filled wells are discarded. then wells are discarded if they are on the corners/edge.
    if plate_last_well is None:
        plate_last_well = len(assay_plate.wells())-1
    # assay_plate_wells = assay_plate.api1wells(plate_first_well,'A2')

    assay_plate_wells = []
    append = False

    for i, well in enumerate(assay_plate.wells()):
        if props[well].get_name() == plate_first_well:
            append = True

        if append:
            assay_plate_wells.append(well)

        if props[well].get_name() == plate_last_well:
            append = False

    if leave_edges_empty:
        leave_corners_empty = False
        # assay_plate_wells = [w for w in assay_plate_wells if w.get_name()[0] not in ['A', 'H'] and w.get_name()[1:] not in ['1', '12']] # for 96 well plates
        assay_plate_wells = [w for w in assay_plate_wells if props[w].get_name()[0] not in ['A', 'P'] and props[w].get_name()[1:] not in ['1', '24']]

    if leave_corners_empty:
        # assay_plate_wells = [w for w in assay_plate_wells if w.get_name()[0:] not in ['A1', 'A12', 'H1', 'H12']] # for 96 well plates
        assay_plate_wells = [w for w in assay_plate_wells if props[w].get_name()[0:] not in ['A1', 'A24', 'P1', 'P24']]

    # check whether enough wells are available
    nwells_needed = (sum([len(target_concentrations[dil]) for dil in target_concentrations]) + int(
        add_zero)) * n_replicates_per_concentration * len(samples)
    if nwells_needed > len(assay_plate_wells):
        msg = '**ERROR** need %d wells for experiment but only %d are free in assay_plate' % (nwells_needed, len(assay_plate_wells))
        print_warning(msg)
        raise Exception(msg)

    # do the dilution for each compound (as each may have different volumes)

    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("**** BUFFER ****")
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line

    dilution_plate_layout = {}
    protein_map = {}  # keys are compound and values will be wells where this specific compound should be added
    done = []

    # here all wells are first filled with the buffer and then with PEG
    for compF in dilution_volumes_pre_compound:
        if compF not in protein_map: protein_map[compF] = []
        # include those that later will have the buffer instead of the actual compound (the leave_n_controls)
        for i, dil in enumerate(target_concentrations):
            dilution_plate_layout = distributed_dilution(small_pipette, large_pipette, adjusted_target_concentrations[compF][dil],
                                                         assay_plate_wells, {dil: compounds_for_dilution[dil]},
                                                         buffer_location[i][0], dilution_volumes_pre_compound[compF],
                                                         dilution_plate_layout=dilution_plate_layout,
                                                         n_replicates=n_replicates_per_concentration, add_zero=add_zero,
                                                         airgap_before_dilution_sample=3, airgap_after_dilution_sample=1,
                                                         change_speeds=([large_pipette, small_pipette], 5, 5), touch_tip_for_dilution=True,
                                                         touch_tip=False, touch_tip_after_aspiration=True)
            for well in dilution_plate_layout:
                if well not in done:
                    protein_map[compF] += [well]
                    done += [well]


    # reverse dictionary to go from (compound_name, concentration_uM) as keys to plate_location as values
    compound_concentration_to_well_by_compF = {}  # values will be locations and intermediate keys tuples with (compound_name,concentration, replicate_index)
    for compF in samples:
        compound_concentration_to_well_by_compF[compF] = {}
        for well in (protein_map[compF]):
            loc = well
            ii = 0
            while tuple(dilution_plate_layout[well]) + (ii,) in compound_concentration_to_well_by_compF[compF]:
                ii += 1
            compound_concentration_to_well_by_compF[compF][tuple(dilution_plate_layout[well]) + (ii,)] = loc

    # add protein(s) to each well one by one

    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("**** PROTEINS ****")
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line
    protocol.comment("")  # empty line

    change_tip_every_concentration = True
    final_assay_plate_dilution_concentration = {}
    speed_changed = False

    for i, compF in enumerate(samples):
        lastsource = None
        lastconc = None
        for tup_nam_conc_rep in sorted(compound_concentration_to_well_by_compF[compF]):
            well = compound_concentration_to_well_by_compF[compF][tup_nam_conc_rep]  # destination well
            vol = proteins_volumes[compF]
            if tup_nam_conc_rep[-1] < leave_n_controls: # Adds buffer instead of protein for the controls
                protocol.comment("Adding control buffer to %s (volume %g uL as for compound %s)" % (str(well), vol, compF))
                source = buffer_location[i][0]
                name = 'Control'
            else:
                source = samples[compF][0]
                name = compF + ('%guM' % (final_protein_concentration))
                protocol.comment("Adding %g uL of compound %s from %s to %s" % (vol, compF, repr(source), str(well)))

                change_pipette_speed([small_pipette, large_pipette], 2, 2) # Change aspiration and dispension speed for protein transfer
                speed_changed = True
            if well in final_assay_plate_dilution_concentration:
                print_warning("**ERROR** already added compound to assay plate well %s adding %s now" % (str(well), compF))
            if vol < 50:
                if lastsource is None or lastsource != source:
                    if small_pipette.hw_pipette['has_tip']: small_pipette.drop_tip()
                    small_pipette.pick_up_tip()
                elif change_tip_every_concentration and lastconc != tup_nam_conc_rep[1]:
                    if small_pipette.hw_pipette['has_tip']: small_pipette.drop_tip()
                    small_pipette.pick_up_tip()
                optimized_transfer(small_pipette, vol, source, well, air_gap_before=5, mix_before=False,
                                   mix_after=(3, min([100, final_volume_per_well / 2.])-2, 1), blow_out=True,
                                   touch_tip_on_blow_out=True,
                                   new_tip='always', second_touch_on_blow_out=-3)
            else:
                if lastsource is None or lastsource != source:
                    if large_pipette.hw_pipette['has_tip']: large_pipette.drop_tip()
                    large_pipette.pick_up_tip()
                elif change_tip_every_concentration and lastconc != tup_nam_conc_rep[1]:
                    if large_pipette.hw_pipette['has_tip']: large_pipette.drop_tip()
                    large_pipette.pick_up_tip()
                optimized_transfer(large_pipette, vol, source, well, air_gap_before=5, mix_before=False,
                                   mix_after=(3, min([40, final_volume_per_well / 2.])-2, 1), blow_out=True,
                                   touch_tip_on_blow_out=True, new_tip='always')
            final_assay_plate_dilution_concentration[well] = [name + '+' + dilution_plate_layout[well][0],
                                                              numpy.round(dilution_plate_layout[well][1] *
                                                              dilution_volumes_pre_compound[compF] / (
                                                                      dilution_volumes_pre_compound[
                                                                           compF]  + vol),2),
                                                              props[well].properties['filled_volume']]
            lastsource = source
            lastconc = tup_nam_conc_rep[1]

            if speed_changed: # Change pipette speed back to default in case it was changed for protein transfer
                change_pipette_speed([small_pipette, large_pipette], default=True)

    if small_pipette.hw_pipette['has_tip']: small_pipette.drop_tip()
    if large_pipette.hw_pipette['has_tip']: large_pipette.drop_tip()
    protocol.comment('')  # empty lines
    protocol.comment('')

    print_plate_layout(dilution_plate_layout, plate_name='INTERMEDIATE DILUTION PLATE before adding compound',
                       comments='intermediate', concentration_unit=concentration_unit, name_index=0, concentration_index=1,
                       round_concentration=3, volume_index=None, delimiter=',', print_also_python_dict=True)

    # print used volumes
    protocol.comment('')
    protocol.comment('')
    protocol.comment("USED VOLUMES:")
    for comp in compounds_for_dilution:
        protocol.comment('dilution compound %s used %g uL    [left ~%g uL stock concentration %g]' % (
        comp, compounds_for_dilution[comp][2] - props[compounds_for_dilution[comp][0]].properties['filled_volume'],
        props[compounds_for_dilution[comp][0]].properties['filled_volume'], compounds_for_dilution[comp][1]))
    for comp in samples:
        protocol.comment('compound %s used %g uL    [left ~%g uL stock concentration %g]' % (
            comp, samples[comp][2] - props[samples[comp][0]].properties['filled_volume'],
            props[samples[comp][0]].properties['filled_volume'], samples[comp][1]))


    protocol.comment('')  # empty lines
    protocol.comment('')
    layout_csv = print_plate_layout(final_assay_plate_dilution_concentration, plate_name='PLATE LAYOUT', comments='final',
                       concentration_unit=concentration_unit, name_index=0, concentration_index=1, round_concentration=3,
                       volume_index=None, delimiter=',', print_also_python_dict=True)


    # PRINT COLLATED WARNINGS AND ERRORS IF ANY
    if len(collated_warnings) > 0:
        protocol.comment("")  # empty line
        protocol.comment("")  # empty line
        protocol.comment("**** %d WARNINGS OR ERRORS GENERATED ****" % (len(collated_warnings)))
        protocol.comment("These are printed here but also above in the log at the point where they have been issued:")
        for warn in collated_warnings:
            protocol.comment("%s" % (warn))
        protocol.comment("")  # empty line
    else:
        protocol.comment("")  # empty line
        protocol.comment("No warnings or errors generated")

    protocol.home()  # home the robot on all axis

    return
'''
simulate with:

python3
import opentrons.simulate
!opentrons_simulate Protocols/PEG_Assay_Step1.py -L /Users/User/Library/Application\ Support/Opentrons/labware/
-L necessary to add custom labware

'''



