"""This script takes the raw datafile from a plate reader (either Excel or ASCII format) and makes it more readable for
the user and also includes information from the plate layout to relate the data to the correct condition. Furthermore,
using a savgol filter it smooths the data to reduce the noise.
"""

import pandas as pd
import numpy as np
import re
from scipy.signal import savgol_filter


def load_data(filepath, filename, skiprows_excel=11, skiprows_txt=7):
    """Load raw data from plate readers, removes unnecessary rows and columns and
    renames columns.

    Parameters
    ----------
    filepath
        path were the file is saved
    filename
        name of the file including extension
    skiprows_excel/skiprows_txt
        number of rows that contain meta information not needed for data analysis
        varies from plate reader to plate reader


    Returns
    -------
    data
        dataframe with sorted raw data
    """

    if filename[-4:] == 'xlsx':
        data = pd.read_excel(filepath + filename, skiprows=[i for i in range(skiprows_excel)])
        data = data.rename(columns={'Unnamed: 0': 'Well_Letter', 'Unnamed: 1': 'Well_Number'})
        data = data.rename(columns={'Well\nRow' : 'Well_Letter', 'Well\nCol': 'Well_Number'})

    elif filename[-3:] == 'txt':
        data = pd.read_csv(filepath + filename, sep=',', skiprows=[i for i in range(skiprows_txt)])
        data = data.rename(columns={'Unnamed: 0': 'Well_Letter', 'Unnamed: 1': 'Well_Number'})
        data = data.rename(columns={'Well Row' : 'Well_Letter', 'Well Col': 'Well_Number'})

    else:
        msg = 'File Format not recognised. Please load in an .xlsx or .txt file.'
        raise Exception(msg)

    data = data.dropna(axis=1)

    return data


def load_layout(data, plate_layout, peg_unit='%'):
    """Takes raw data from load_data function and adds information about PEG concentration from plate layout.

    Parameters
    ----------
    data
        dataframe containing raw data from plate reader pre-processed by load_data function
    plate_layout
        csv file representing the plate layout - can come from make_plate_layout script
    peg_unit
        string that represents the unit which was used for PEG, usually "%"

    Returns
    -------
    data
        dataframe with adjusted data; if more than one sample was used returns list of adjusted data and
        saves individual dataframes
    """

    data.insert(2, 'PEG_Concentration', 0)
    data.insert(3, 'Protein', 'Control')

    # This re reads out the content of the well, i.e. whether it's a control and the concentration of PEG
    search_string = re.compile('(.+)[\+| ] *.*? ([\d]+.*[\d]*)' + peg_unit)

    # Adds information about each well regarding PEG concentration and Controls/Samples to each well

    for i, row in data.iterrows():
        well_letter = data.loc[i, 'Well_Letter']
        well_number = data.loc[i, 'Well_Number']

        # Find well in plate_layout - for two different versions of plate layouts
        if '0' in plate_layout.columns:
            target_string = plate_layout.loc[plate_layout['0'] == well_letter, str(well_number)].iloc[0]
        else:
            target_string = plate_layout.loc[plate_layout['plate_slot_'] == well_letter, str(well_number)].iloc[0]

        hit = search_string.search(str(target_string))

        if hit:
            data.loc[i, 'Protein'] = hit.group(1)
            data.loc[i, 'PEG_Concentration'] = float(hit.group(2))

    data = data.sort_values(by=['PEG_Concentration', 'Protein', 'Well_Number', 'Well_Letter'])

    # Split up in case there is more than one sample in one file
    n_samples = np.unique(data['Protein'])

    if len(n_samples) == 2 or len(n_samples) == 1:  # 2 means one sample + control, 1 means either only control or only sample

        return data

    elif len(n_samples) == 3 or len(n_samples) == 4 or len(n_samples) == 5:
        control_data = data[(data['Protein'] == 'Control')]
        correct_n_samples = np.delete(n_samples, np.where(n_samples == 'Control'))

        adjusted_data = []
        sample_1 = data[(data['Protein'] == correct_n_samples[0])]
        sample_1_con = pd.concat([sample_1, control_data])
        sample_1_con = sample_1_con.sort_values(by=['PEG_Concentration', 'Protein', 'Well_Number', 'Well_Letter'])
        adjusted_data.append(sample_1_con)
        sample_1_con.to_csv(filepath + filename[:-4] + '_' + correct_n_samples[0] + '.csv', index=False)

        sample_2 = data[(data['Protein'] == correct_n_samples[1])]
        sample_2_con = pd.concat([sample_2, control_data])
        sample_2_con = sample_2_con.sort_values(by=['PEG_Concentration', 'Protein', 'Well_Number', 'Well_Letter'])
        adjusted_data.append(sample_2_con)
        sample_2_con.to_csv(filepath + filename[:-4] + '_' + correct_n_samples[1] + '.csv', index=False)

        if len(n_samples) == 4 or len(n_samples) == 5:
            sample_3 = data[(data['Protein'] == correct_n_samples[2])]
            sample_3_con = pd.concat([sample_3, control_data])
            sample_3_con = sample_3_con.sort_values(by=['PEG_Concentration', 'Protein', 'Well_Number', 'Well_Letter'])
            adjusted_data.append(sample_3_con)
            sample_3_con.to_csv(filepath + filename[:-4] + '_' + correct_n_samples[2] + '.csv', index=False)

            if len(n_samples) == 5:
                sample_4 = data[(data['Protein'] == correct_n_samples[3])]
                sample_4_con = pd.concat([sample_4, control_data])
                sample_4_con = sample_4_con.sort_values(by=['PEG_Concentration', 'Protein', 'Well_Number', 'Well_Letter'])
                adjusted_data.append(sample_4_con)
                sample_4_con.to_csv(filepath + filename[:-4] + '_' + correct_n_samples[3] + '.csv', index=False)

        return adjusted_data

    else:
        msg = '***ERROR*** The function has not been implemented for more than 3 samples.'
        raise Exception(msg)


#####################################################################################################################
############################ Information that needs to be provided before running ###################################
#####################################################################################################################

# File information for raw data
filepath_1 = 'Example files/'
filepath_2 = '20_07_31_HzATNP_pH7/'
filepath = filepath_1 + filepath_2

filename = 'After_Centrifugation.txt'

# File information for plate layout
plate_filepath = 'Example files/Plate_Layouts/Supernatant/' # for analysis of soluble fraction and intrinsic fluorescence
# plate_filepath = 'Example files/Plate_Layouts/Original_Plate/' # for analysis of turbidity
plate_filename = '2021_05_13_HzATNP_pH7.csv'

# Information on where the data should be saved

if '.txt' in filename:
    save_name = filename[:-4]
else:
    save_name = filename[:-5]


#####################################################################################################################
######################################## No need to adjust below here ###############################################
#####################################################################################################################

# Properly reads data and deletes useless columns and rows
if 'endpoint' in filename:
    data = load_data(filepath, filename, skiprows_excel=10, skiprows_txt=6)
elif filepath_2 == 'Gua/':
    data = load_data(filepath, filename, skiprows_txt=8)
else:
    data = load_data(filepath, filename)

plate_layout = pd.read_csv(plate_filepath + plate_filename)

# Adds information about concentrations and controls to file
data = load_layout(data, plate_layout, peg_unit='%')
single = False

if filename[0] !='I' and filename[0] !='F':
    # Apply Savgol filter to smooth the data and reduce the noise but only if it's not intrinsic fluorescence or free fluorophore data
    if type(data) is not list:
        data = [data]
        single = True

    for i, d in enumerate(data):
        new_data = pd.DataFrame(savgol_filter(d.iloc[:,5:], 11, 3, axis=1), columns=d.iloc[:, 5:].columns, index=d.iloc[:, 5:].index)
        final = d.iloc[:,0:5].join(new_data)
        if single:
            sn = save_name + '_adjusted.csv'
        else:
            sn = save_name + '_' + str(i+1) + '_adjusted.csv'
        final.to_csv(filepath + sn, index=False)

else:
    if type(data) is not list:
        data = [data]
        single = True

    for i, d in enumerate(data):
        if single:
            sn = save_name + '_adjusted.csv'
        else:
            sn = save_name + '_' + str(i + 1) + '_adjusted.csv'
        d.to_csv(filepath + sn, index=False)