"""
Produces a plate layout in csv format from the dictionary that the OpenTrons robot gives out in its log (when run
with PEG_Assay or Supernatant protocols).
"""

import pandas as pd
from datetime import datetime
import numpy as np

def print_plate_layout(platelayout, is384=True, source_plate=None, unit='%'):
    """Makes a dataframe out of plate layout dictionary.

    Parameters
    ----------
    platelayout
        dictionary copied from log from opentrons software. With keys representing
        wells (e.g. 'C12') and values containing information about the content
        (e.g. ProteinX + PEG 23%)
    is384
        true if 384 wells plate are use, false for 96 well plate
    source_plate
        dataframe containing plate layout from source plate. Needed
        if plate layout for supernatant is made

    Returns
    -------
    df
        dataframe with plate layout


    platelayout
        dictionary with keys representing wells (e.g. 'C12') and values containing information about the content
        (e.g. ProteinX + PEG 23%)
    """

    # Create empty dataframe with proper rows and columns
    rows = ['A','B','C','D','E','F','G','H']
    columns = [i for i in range(13)]

    if is384:
        rows.extend(['I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'])
        columns.extend([i for i in range(13, 25)])

    df = pd.DataFrame(columns=columns)
    df[0] = rows


    if source_plate is not None:
        source_plate = source_plate.fillna('')

        # Replace information in platelayout dict (e.g. 'L3') with data from source_plate
        for well in platelayout:
            source_letter = platelayout[well][0]
            source_number = platelayout[well][1:]
            value = source_plate.loc[source_plate['0']==source_letter, str(source_number)].iloc[0]
            df.loc[df[0]==well[0], int(well[1:])] = value

    else:
        for well in platelayout:

            content = platelayout[well][0] + ' ' + str(np.round(platelayout[well][1], 3)) + unit

            df.loc[df[0]==well[0], int(well[1:])] = content

    return df



#####################################################################################################################
############################ Information that needs to be provided before running ###################################
#####################################################################################################################

# Copy dictionary from log output from OpenTrons software, either simulated or from the robot directly

# example for step 1:
layout = dict([("A4", ['Control+PEG15', 0.0]),("A5", ['HzATNP 1uM+PEG15', 9.0]),("A6", ['HzATNP 1uM+PEG30', 20.0]),("B4", ['HzATNP 1uM+PEG15', 0.0]),("B5", ['HzATNP 1uM+PEG15', 9.0]),("B6", ['Control+PEG50', 23.0]),("C4", ['HzATNP 1uM+PEG15', 0.0]),("C5", ['Control+PEG30', 11.0]),("C6", ['HzATNP 1uM+PEG50', 23.0]),("D4", ['Control+PEG15', 3.0]),("D5", ['HzATNP 1uM+PEG30', 11.0]),("D6", ['HzATNP 1uM+PEG50', 23.0]),("E4", ['HzATNP 1uM+PEG15', 3.0]),("E5", ['HzATNP 1uM+PEG30', 11.0]),("E6", ['Control+PEG50', 26.0]),("F4", ['HzATNP 1uM+PEG15', 3.0]),("F5", ['Control+PEG30', 13.0]),("F6", ['HzATNP 1uM+PEG50', 26.0]),("G4", ['Control+PEG15', 5.0]),("G5", ['HzATNP 1uM+PEG30', 13.0]),("G6", ['HzATNP 1uM+PEG50', 26.0]),("H4", ['HzATNP 1uM+PEG15', 5.0]),("H5", ['HzATNP 1uM+PEG30', 13.0]),("I4", ['HzATNP 1uM+PEG15', 5.0]),("I5", ['Control+PEG30', 15.0]),("J4", ['Control+PEG15', 6.0]),("J5", ['HzATNP 1uM+PEG30', 15.0]),("K4", ['HzATNP 1uM+PEG15', 6.0]),("K5", ['HzATNP 1uM+PEG30', 15.0]),("L4", ['HzATNP 1uM+PEG15', 6.0]),("L5", ['Control+PEG30', 17.0]),("M4", ['Control+PEG15', 7.0]),("M5", ['HzATNP 1uM+PEG30', 17.0]),("N4", ['HzATNP 1uM+PEG15', 7.0]),("N5", ['HzATNP 1uM+PEG30', 17.0]),("O4", ['HzATNP 1uM+PEG15', 7.0]),("O5", ['Control+PEG30', 20.0]),("P4", ['Control+PEG15', 9.0]),("P5", ['HzATNP 1uM+PEG30', 20.0])]) 
#example for step 2:
layout = dict([("A10", "C5"),("A6", "G6"),("A7", "K5"),("A8", "O4"),("A9", "C4"),("B10", "C5"),("B6", "G6"),("B7", "K5"),("B8", "O4"),("B9", "C4"),("C10", "F5"),("C6", "F6"),("C7", "J5"),("C8", "N4"),("C9", "B4"),("D10", "F5"),("D6", "F6"),("D7", "J5"),("D8", "N4"),("D9", "B4"),("E10", "I5"),("E6", "D6"),("E7", "H5"),("E8", "L4"),("E9", "A4"),("F10", "I5"),("F6", "D6"),("F7", "H5"),("F8", "L4"),("F9", "A4"),("G10", "L5"),("G6", "C6"),("G7", "G5"),("G8", "K4"),("G9", "D4"),("H10", "L5"),("H6", "C6"),("H7", "G5"),("H8", "K4"),("H9", "D4"),("I10", "O5"),("I6", "A6"),("I7", "E5"),("I8", "I4"),("I9", "G4"),("J10", "O5"),("J6", "A6"),("J7", "E5"),("J8", "I4"),("J9", "G4"),("K10", "B6"),("K6", "P5"),("K7", "D5"),("K8", "H4"),("K9", "J4"),("L10", "B6"),("L6", "P5"),("L7", "D5"),("L8", "H4"),("L9", "J4"),("M10", "E6"),("M6", "N5"),("M7", "B5"),("M8", "F4"),("M9", "M4"),("N10", "E6"),("N6", "N5"),("N7", "B5"),("N8", "F4"),("N9", "M4"),("O6", "M5"),("O7", "A5"),("O8", "E4"),("O9", "P4"),("P6", "M5"),("P7", "A5"),("P8", "E4"),("P9", "P4")]) 

# Name and Path for saving the plate layout
filename = 'HzATNP_pH7'
filepath = 'Example files/Plate_Layouts/'

sn = True # True if dictionary comes from the PEG_Assay_Step2 script

unit = '%'

# Below only necessary if plate_layout is from Supernatant protocol
source_plate_filepath = 'Example files/Plate_Layouts/Original_Plate/'
source_plate_filename = '2021_05_13_HzATNP_pH7.csv'

#####################################################################################################################
######################################## No need to adjust below here ###############################################
#####################################################################################################################

if sn:
    source_plate = pd.read_csv(source_plate_filepath+source_plate_filename)
    layout_csv = print_plate_layout(layout, source_plate=source_plate, unit=unit)
    layout_csv.to_csv(filepath + 'Supernatant/' + datetime.today().strftime('%Y_%m_%d') + '_' + filename + '.csv', index=False)

else:
    layout_csv = print_plate_layout(layout, unit=unit)
    layout_csv.to_csv(filepath + 'Original_Plate/' + datetime.today().strftime('%Y_%m_%d') + '_' + filename + '.csv', index=False)
